//
//  studentRecord.h
//  HW6CS
//
//  Created by Steven Francus on 11/1/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef HW6CS_studentRecord_h
#define HW6CS_studentRecord_h
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <algorithm>

class studentRecord
{
public:
    studentRecord();
    studentRecord(const studentRecord & student);
    studentRecord(std::string, std::string);
    ~studentRecord();
    bool operator<(const studentRecord & other) const;
    bool operator>(const studentRecord & other) const;
    studentRecord& operator=(const studentRecord& other);
    std::string print() const;
    std::string getName() const;
    void setName(std::string);
    std::string getID() const;
    void setID(std::string);
    private:
    std::string studentName;
    std::string studentID;
};
#endif
