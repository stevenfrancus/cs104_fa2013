//
//  ListArray.h
//  HW4CS
//
//  Created by Steven Francus on 9/29/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef HW4CS_ListArray_h
#define HW4CS_ListArray_h
#include "AbstractList.h"
template <class T>
class ListArray: public List<T>
{
protected:
    T* _array;
    int size_of;
    int numElem;
    virtual void expandArray() = 0;
public:
    ListArray();
    virtual ~ListArray();
    virtual void insert (int pos, const T & item);
    virtual void remove (int pos);
    virtual void set (int pos, const T & item);
    T const & get (int pos) const;
};
template <class T>
ListArray<T>::ListArray() : _array(NULL), size_of(0), numElem(0)
{
    
}
template <class T>
void ListArray<T>::insert(int pos, const T & item)
{
    if(pos > size_of || pos < 0)
    {
        std::cout << "Position does not exist." << std::endl;
        return;
    }
    
    if(numElem + 1 > size_of)
    {
        expandArray();
    }
    
    for(int i = numElem; i > pos; --i)
    {
        _array[i] = _array[i-1];
    }
    
     _array[pos] = item;
    ++numElem;
    
}
template <class T>
ListArray<T>::~ListArray()
{
    delete []_array;
}
template <class T>
void ListArray<T>::remove(int pos)
{
    if(pos > size_of || pos < 0)
    {
        std::cout << "Position does not exist." << std::endl;
        return;
    }
    for(int i = pos; i < numElem - 1; ++i)
    {
        _array[i] = _array[i+1];
    }
    --numElem;
}
template <class T>
void ListArray<T>::set(int pos, const T & item)
{
    _array[pos] = item;
}
template <class T>
T const & ListArray<T>::get (int pos) const
{
    return _array[pos];
}
#endif
