//
//  Heap.h
//  HW6CS
//
//  Created by Steven Francus on 10/30/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef HW6CS_Heap_h
#define HW6CS_Heap_h
#include "VectorKnockoff.h"
#include <stdexcept>
template <class T>
class Heap
{
public:
    T const & peek() const throw(std::exception);
    void remove() throw(std::exception);
    void add(const T & item);

    Heap (int, bool);
    ~Heap();

private:
    void swap(int, int);
    void trickleUp(int);
    void trickleDown(int);
    void heapRebuild(int);
    bool maxOrMin;
    int nodeCount;
    int total_nodes;
    List<T>* heapHolder;
};
template <class T>
Heap<T>::Heap(int _nodeCount, bool _minOrMaxRoot) : nodeCount(_nodeCount), maxOrMin(_minOrMaxRoot), total_nodes(0)
//by my long variable names you should know I programmed Objective-C
{
    heapHolder = new VectorKnockoff<T>();
}
template <class T>
Heap<T>::~Heap()
{
    delete heapHolder;
}
template <class T>
void Heap<T>::add(const T& item)
{
    heapHolder->insert(total_nodes, item);
    int pos = total_nodes;
    if((pos-1)/nodeCount >= 0)
    {
        trickleUp(pos);
    }
    total_nodes++;
};
template <class T>
T const & Heap<T>::peek() const throw(std::exception)
{
    if(total_nodes==0)    {
        throw std::logic_error("Empty heap.");
    }
    return heapHolder->get(0);
};
template <class T>
void Heap<T>::swap(int pos1, int pos2)
{
    T temp = heapHolder->get(pos1);
    heapHolder->set(pos1, heapHolder->get(pos2));
    heapHolder->set(pos2, temp);
}
template <class T>
void Heap<T>::remove() throw(std::exception)
{
    if(!total_nodes) {
        throw std::logic_error("Empty heap.");
    }
    T root = heapHolder->get(0);
    heapHolder->set(0, heapHolder->get(--total_nodes));
    trickleDown(0);
    
}
template <class T>
void Heap<T>::trickleUp(int nullu)
{
    if((nullu-1)/nodeCount >= 0)
    {
        if(maxOrMin)
        {
            if(heapHolder->get(nullu) > heapHolder->get((nullu-1)/nodeCount))
            {
                swap(nullu, (nullu-1)/nodeCount);
                nullu = (nullu-1)/nodeCount;
                trickleUp(nullu);
            }
        }
        else
        {
            if(heapHolder->get(nullu) < heapHolder->get((nullu-1)/nodeCount))
            {
                swap(nullu, (nullu-1)/nodeCount);
                nullu = (nullu-1)/nodeCount;
                trickleUp(nullu);
            }
        }
    }
}
template <class T>
void Heap<T>::trickleDown(int root)
{
    int child = nodeCount*root + 1; //check if there exists at least a left most node on the next level
    if(child < total_nodes)
    {
        for(int i = 1; i < nodeCount; i++) // cycles through all nodes, but breaks if the node is null meaning no more nodes that level (saves time)
        {
            int nextChild = child+i;
            if(nextChild < total_nodes)
            {
                if(maxOrMin) // if greater than heap property
                {
                    if(heapHolder->get(nextChild) > heapHolder->get(child))
                        child = nextChild;
                }
                else // if less than heap property
                {
                    if(heapHolder->get(nextChild) < heapHolder->get(child))
                        child = nextChild;
                }
            }
        }
        if(maxOrMin)
        {
            if(heapHolder->get(0) < heapHolder->get(child))
            {
                swap(0, child);
                trickleDown(child);
            }
        }
        else
        {
            if(heapHolder->get(0) > heapHolder->get(child))
            {
                swap(0, child);
                trickleDown(child);
            }
        }
        
    }
}

#endif
