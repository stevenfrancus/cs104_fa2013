//
//  main.cpp
//  HW6CS
//
//  Created by Steven Francus on 10/30/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include <iostream>
#include "Heap.h"
#include "studentRecord.h"
#include <cmath>
#include <ctime>
#include <stdio.h>      
#include <stdlib.h>  
#include <time.h>
using namespace std;
int main(int argc, const char * argv[])
{

    Heap<studentRecord> heap(4, false);
    while(true)
    {
    std::cout << "Would you like to:\n1. add a student record\n2. remove a student record (the student who's name is alphabetically first)\n3. exit?\n";
    int choice = 0;
    cin >> choice;
    cin.ignore();
    if(choice == 1)
    {
        std::string studentNam, studentI;
        std::cout << "What is the student's name?" << std::endl;
        std::getline(std::cin, studentNam);
        std::cout << "What is the student's ID number?" << std::endl;
        std::getline(std::cin, studentI);
        studentRecord newStudent(studentNam, studentI);
        try{
        heap.add(newStudent);
        }
        catch(std::exception &e) { std::cout << e.what() << std::endl; }
        
    }
    else if(choice == 2)
    {
        std::cout << "This operation will delete the person with the alphabetically first name." << std::endl;
        try {
            heap.remove();
        }
        catch(std::exception &e)
        {
            std::cout << e.what() << std::endl;
        }
    }
    else
    {
        break;
    }
    std::cout << "The person who is now first alphabetically is: " << std::endl;
    try {
        std::cout << heap.peek().print() << std::endl;
    }
    catch(std::exception &e)
    {
        std::cout << e.what() << std::endl;
    }
    }
    return 0;
}

