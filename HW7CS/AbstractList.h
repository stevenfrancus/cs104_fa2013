//
//  AbstractList.h
//  HW4CS
//
//  Created by Steven Francus on 9/26/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef HW4CS_AbstractList_h
#define HW4CS_AbstractList_h
template <class T>
class List
{
public:
    virtual ~List() {}
    virtual void insert (int pos, const T & item) = 0;
    virtual void remove (int pos) = 0;
    virtual void set (int pos, const T & item) = 0;
    virtual T const & get (int pos) const = 0;
};

#endif
