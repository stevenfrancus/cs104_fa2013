//
//  VectorKnockoff.h
//  HW4CS
//
//  Created by Steven Francus on 9/29/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef HW4CS_VectorKnockoff_h
#define HW4CS_VectorKnockoff_h
#include "ListArray.h"
template <class T>
class VectorKnockoff : public ListArray<T>
{
public:
    VectorKnockoff();
    virtual ~VectorKnockoff();
protected:
    virtual void expandArray();
    using ListArray<T>::_array;
    using ListArray<T>::size_of;
    using ListArray<T>::numElem;
};
template <class T>
VectorKnockoff<T>::VectorKnockoff()
{
}
template <class T>
VectorKnockoff<T>::~VectorKnockoff<T>()
{
}
template <class T>
void VectorKnockoff<T>::expandArray()
{
    int newsize = 0;
    if(!size_of) newsize = 1;
    else
    {
        newsize=size_of*2;
    }
    T* arraytwo = new T[newsize];
    for(int i = 0; i < numElem; i++)
    {
        arraytwo[i] = _array[i];
    }
    delete []_array;
    _array = arraytwo;
    size_of = newsize;
}
#endif
