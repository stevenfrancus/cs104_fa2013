//
//  studentRecord.cpp
//  HW6CS
//
//  Created by Steven Francus on 11/1/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include "studentRecord.h"
studentRecord::studentRecord() : studentName(""), studentID("")
{
};
studentRecord::studentRecord(std::string name, std::string id) : studentName(name), studentID(id)
{
    
};
studentRecord::studentRecord(const studentRecord& at): studentName(at.getName()), studentID(at.getID())
{
    
}

studentRecord::~studentRecord()
{
    
};
studentRecord& studentRecord::operator=(const studentRecord& other)
{
    if( this == &other )
        return *this;
    this->studentName = other.studentName;
    this->studentID = other.studentID;
    return *this;
}
bool studentRecord::operator<(const studentRecord & other) const
{
    int traverseStrings = 0;
    std::string thisName = this->studentName;
    std::string otherName = other.studentName;
    std::transform(thisName.begin(), thisName.end(), thisName.begin(), ::tolower);
    std::transform(otherName.begin(), otherName.end(), otherName.begin(), ::tolower);
    int minLength = (int)std::min(thisName.length(), otherName.length());
    while (traverseStrings < minLength)
    {
        if (thisName[traverseStrings] < otherName[traverseStrings]) return true;
        else if (thisName[traverseStrings] > otherName[traverseStrings]) return false;
        else
            ++traverseStrings;
    }
    return false; //this probably means the strings are equal
};
bool studentRecord::operator>(const studentRecord & other) const
{
    return (this->studentName!=other.studentName && !(*this<other));
};
std::string studentRecord::print() const
{
    return "Student name:\n" + studentName + "\nStudent ID: " + studentID;
};
std::string studentRecord::getName() const
{
    return studentName;
};
void studentRecord::setName(std::string nomen)
{
    studentName = nomen;
};
std::string studentRecord::getID() const
{
    return studentID;
};
void studentRecord::setID(std::string set)
{
    studentID = set;
};

