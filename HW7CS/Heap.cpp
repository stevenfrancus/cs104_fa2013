//
//  Heap.h
//  HW6CS
//
//  Created by Steven Francus on 10/30/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef HW6CS_Heap_h
#define HW6CS_Heap_h
#include "Heap.h"
template <class T>
Heap<T>::Heap(int _nodeCount, bool _minOrMaxRoot) : nodeCount(_nodeCount), maxOrMin(_minOrMaxRoot), total_nodes(0)
//by my long variable names you should know I programmed Objective-C
{
    heapHolder = new VectorKnockoff<T>();
}
template <class T>
Heap<T>::~Heap()
{
    delete heapHolder;
}
template <class T>
void Heap<T>::add(const T& item)
{
    heapHolder->insert(total_nodes, item);
    int pos = total_nodes;
    int pos_parent = (pos-1)/nodeCount;
    if(pos_parent >= 0)
    {
        trickleUp(pos);
    }
    total_nodes++;
};
template <class T>
T const & Heap<T>::peek() const throw(std::exception)
{
    T temp;
    if(total_nodes==0)    {
        throw std::logic_error("Empty heap.");
    }
    return heapHolder->get(0);
};
template <class T>
void Heap<T>::swap(int pos1, int pos2)
{
    T temp;
    temp = heapHolder->get(pos1);
    heapHolder->set(pos1, heapHolder->get(pos2));
    heapHolder->set(pos2, temp);
}
template <class T>
void Heap<T>::remove() throw(std::exception)
{
    if(!total_nodes) {
        throw std::logic_error("Empty heap.");
    }
    T root = heapHolder->get(0);
    heapHolder->set(0, heapHolder->get(--total_nodes));
    trickleDown(0);
    
}
template <class T>
void Heap<T>::trickleUp(int nullu)
{
    if((nullu-1)/nodeCount >= 0)
    {
        if(maxOrMin)
        {
            if(heapHolder->get(nullu) > heapHolder->get((nullu-1)/nodeCount))
            {
                swap(nullu, (nullu-1)/nodeCount);
                nullu = (nullu-1)/nodeCount;
                trickleUp(nullu);
            }
        }
        else
        {
            if(heapHolder->get(nullu) < heapHolder->get((nullu-1)/nodeCount))
            {
                swap(nullu, (nullu-1)/nodeCount);
                nullu = (nullu-1)/nodeCount;
                trickleUp(nullu);
            }
        }
    }
}
template <class T>
void Heap<T>::trickleDown(int root)
{
    int child = nodeCount*root + 1;
    if(child < total_nodes)
    {
        int children[nodeCount-1];
        for(int i = 1; i < nodeCount; i++)
        {
            int nextChild = child+i;
            if(nextChild < total_nodes)
            {
                if(maxOrMin)
                {
                    if(heapHolder->get(nextChild) > heapHolder->get(child))
                        child = nextChild;
                }
                else
                {
                    if(heapHolder->get(nextChild) < heapHolder->get(child))
                        child = nextChild;
                }
            }
        }
        if(maxOrMin)
        {
            if(heapHolder->get(0) < heapHolder->get(child))
            {
                T temp = heapHolder->get(0);
                heapHolder->set(0, heapHolder->get(child));
                heapHolder->set(child, temp);
                trickleDown(child);
            }
        }
        else
        {
            if(heapHolder->get(0) > heapHolder->get(child))
            {
                T temp = heapHolder->get(0);
                heapHolder->set(0, heapHolder->get(child));
                heapHolder->set(child, temp);
                trickleDown(child);
            }
        }
        
    }
}
#endif
