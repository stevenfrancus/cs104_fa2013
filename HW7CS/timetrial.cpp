//
//  timetrial.cpp
//  
//
//  Created by Steven Francus on 11/4/13.
//
//
#include <iostream>
#include "Heap.h"
#include <cmath>
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main()
{
    clock_t start, finish;
    double dur = 0;
    Heap<int> heap(100, true);
    srand(time(NULL));
    int tenThousand[10000];
    for(int i = 0; i < 10000; i++)
    {
        tenThousand[i] = rand() % 10000;
    }
    for(int i = 0; i < 10000; i++)
    {
        heap.add(tenThousand[i]);
    }
    start = clock();
    for(int i = 0; i < 10000; i++)
    {
        heap.remove();
    }
     
    finish = clock();
    
    dur = (double)(finish-start);
    dur /= CLOCKS_PER_SEC;
    cout << "The time it took to insert 10k elements is: " << dur << " seconds." <<endl;
}