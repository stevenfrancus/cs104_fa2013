#include "Wall.h"
#include "WallPost.h"
#include "Node.h"
#include <sstream>
#include <fstream>
#include <ostream>
Wall::Wall()
{
    wallOf = new LinkedList<WallPost*>();
}
Wall::~Wall()
{
}
int Wall::length() const
{
    return wallOf->length();
}
void Wall::addPost(const WallPost &postem)
{
    WallPost *post = new WallPost(postem.getAuthor(), postem.getBody());
    post->setPostNumber(wallOf->length()+1);
    wallOf->insert(wallOf->length(), post);
} 
void Wall::removePost(int number)
{
    try {
    wallOf->remove(number-1);
    }
    catch(std::exception &e)
    {
        std::cout << e.what() << std::endl;
    }
}
std::string Wall::WallToString(bool readable)
{
    std::string varchar = "";
    for(ListIterator<WallPost*> mu = wallOf->begin(); mu!=wallOf->end(); mu++)
    {
        if(readable)
        {
        (*mu)->printWallPost();
        }
         else
        {
        varchar += (*mu)->formatWallPost();
        }
    }
    return varchar;
}
void Wall::StringToWall(std::string wallstring)
{
    WallPost *append = new WallPost();
    std::stringstream re(wallstring);
    std::string pushback;
    int i = 0;
    while(std::getline(re, pushback, '|'))
    {
        if(i==0)
        {
            append->setAuthor(pushback);
        }
        else
        {
            append->setBody(pushback);
        }
        i++;
    }
    wallOf->insert(wallOf->length(), append);
}
void Wall::readFromFile(std::string filename)
{
    std::ifstream db;
    db.open(filename.c_str(), std::ios::in);
    if(!db) {
        std::cerr << "File could not be opened." << std::endl;
        std::exit(1);
    }
    std::string ks = "";
    while(!db.eof()) {
        getline(db, ks);
        StringToWall(ks);
    }
}
