#include "User.h"
User::User() : username(""), password(""), hometown("")
{
    friends = new Bag<User>();
    pendingFriends = new Bag<User>();
}
User::User(std::string userstring)
{
    std::stringstream re(userstring);
    std::string pushback;
    int i = 0;
    while(std::getline(re, pushback, '.'))
    {
        if(i==0)
            username = pushback;
        else if(i==1)
            password = pushback;
        else
            hometown = pushback;
        i++;
    }
}
User::User(std::string user_name, std::string pass_word) : username(user_name), password(pass_word), hometown("")
{
    friends = new Bag<User>();
    pendingFriends = new Bag<User>();
}
User::User(std::string user_name, std::string pass_word, std::string home_town) : username(user_name), password(pass_word), hometown(home_town)
{
    friends = new Bag<User>();
    pendingFriends = new Bag<User>();
}
User::~User()
{
}
void User::setHometown(std::string aleph)
{
    hometown = aleph;
}
std::string User::getHometown() const
{
    return hometown;
}
std::string User::getUsername() const
{
    return username;
}
void User::setPassword(std::string pass)
{
    password = pass;
}
std::string User::getPassword() const
{
    return password;
}
void User::writePost(std::string postBody)
{
    WallPost alpha(username, postBody);
    wall.addPost(alpha);
}
void User::deletePost(int number)
{
    wall.removePost(number);
}
std::string User::userDataToString() const
{
    return "%" + username + "." + password + "." + hometown + "\n";
}
void User::outputUserData() const
{
    std::cout << "User info:\nUsername: " << username << "\nPassword: " << password <<  "\nHometown: "<< hometown << std::endl;
}
void User::printWall()
{
    wall.WallToString(true);
}
std::string User::writeWallToFile()
{
    return wall.WallToString(false);
}
bool User::operator==(User const & other)
{
    return this->username == other.username;
}
void User::addFriend(const User& user)
{
    friends->push_back(user);
}
void User::removeFriend(User &user)
{
            friends->remove(user);
            user.friends->remove(*this);
}
void User::addPending(const User &user)
{
    pendingFriends->push_back(user);
}
void User::sendFriendRequest(User &user)
{
    if(!user.pendingFriends->contains(*this) && !user.friends->contains(*this))
    {
    user.pendingFriends->push_back(*this);
    }
}
void User::respondToFriendRequest(User &dum, bool response)
{
    if(response)
    {
        if(!friends->contains(dum))
        {
            friends->push_back(dum);
            dum.friends->push_back(*this);
        }
    }
    pendingFriends->remove(dum);
    if(dum.pendingFriends->contains(*this))
    {
        dum.pendingFriends->remove(*this);
    }
}
std::string User::printPending(bool writeOrPrint)
{
    std::string varchar ="";
    for(BagIterator<User> mu = pendingFriends->begin(); mu!=pendingFriends->end(); mu++)
    {
        if(writeOrPrint)
        std::cout << (*mu).getUsername() << std::endl;
        else
        {
            varchar= varchar + getUsername() + ">" + (*mu).getUsername();
            varchar+="\n";
        }
    }
    return varchar;
}
std::string User::printFriends(bool writeOrPrint)
{
    std::string varchar = "";
    for(BagIterator<User> mu = friends->begin(); mu!=friends->end(); mu++)
    {
        if(writeOrPrint)
        {
        std::cout << (*mu).getUsername() << std::endl;
        }
        else
        {
            varchar= varchar + getUsername() + ":" + (*mu).getUsername();
            varchar+="\n";
        }
    }
    return varchar;
}
