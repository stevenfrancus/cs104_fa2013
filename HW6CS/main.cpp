//
//  main.cpp
//  LinkedListTemplate
//
//  Created by Steven Francus on 9/19/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include <iostream>
//#include "linkedlisttemplate.h"
#include "node.h"
#include "Wall.h"
#include "User.h"
#include "UserList.h"
#include "WallPost.h"
#include <fstream>
using namespace std;

int main()
{
   int choice = 0;
    UserList* userlist = new UserList();
    userlist->readListFromFile("example.txt");
    bool endtimescenario = false;
    while(!endtimescenario)
    {
    cout << "Welcome to the Social Network beta! \n Would you like to: \n 1) Log in as a user? \n 2) Create a new user? \n 3) Quit the program?\n";
    cin >> choice;
        cin.ignore();
    if(cin)
    {
        switch(choice)
        {
            case 1:
            {
                string username = "";
                string password = "";
                bool loggedin = false;
                cout << "You selected: log in as a user. To log in as a user, please enter your username now.\n";
                getline(cin, username);
                for(BagIterator<User> mu = userlist->begin(); mu != userlist->end(); mu++)
                {
                    if((*mu).getUsername()==username)
                    {
                        cout << "Please enter your password now.\n";
                        getline(cin, password);
                        if((*mu).getPassword()==password)
                        {
                            loggedin = true;
                            userlist->socialnetwork((*mu));
                            userlist->writeListToFile("example.txt");
                        }
                    }
                }
                if(!loggedin)
                {
                    cout << "Log in failed. Maybe your password or username was not correct.\n";
                }
                break;
            }
            case 2:
            {
                User user;
                string username, password, hometown;
                cout << "You selected: create new user. To create a new user, please enter a username to assign the new user." << endl;
                getline(cin, username);
                cin.clear();
                if(userlist->findUser(username, user))
                {
                    cout << "This username is already taken. Please enter a new username.\n";
                    getline(cin, username);
                    cin.ignore();
                }
                cout << "Please assign a password for the new user." << endl;
                getline(cin, password);
                cin.clear();
                cout << "Please give a hometown for the new user. You may leave this blank if you wish.\n";
                getline(cin, hometown);
                User newUser(username, password, hometown);
                userlist->addUser(newUser);
                userlist->writeListToFile("example.txt");
                break;
            }
            case 3:
            {
                endtimescenario = true;
                break;
            }
            default:
            {
                cout << "Input is incorrect." << endl;
                endtimescenario = true;
                break;
            }
        }
    }
    else
    {
        
    }
    }
}


