//
//  ListIterator.h
//  
//
//  Created by Steven Francus on 10/28/13.
//
//

#ifndef _ListIterator_h
#define _ListIterator_h
#include "Node.h"
#include <iterator>
template <class T>
class ListIterator: public std::iterator<std::bidirectional_iterator_tag, T>
{
public:
    ListIterator(const List<T> *list, Node<T>* node);
    bool operator!=(ListIterator const & other);
    bool operator==(ListIterator const & other);
    ListIterator operator++(int);
    ListIterator& operator++();
    const T & operator*();
    friend class List<T>;
private:
    const List<T> *container;
    Node<T> *node;
};
#endif