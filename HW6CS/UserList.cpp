#include <string>
#include "UserList.h"
#include <fstream>
#include <iostream>
#include <istream>
#include <sstream>
#include <string>
#include <algorithm>
#include <exception>
int firstIndexOfChar(std::string str, char character)
{
    for(int i = 0; i < str.length(); i++)
    {
        if(str[i]==character)
        {
            return i;
        }
    }
    return -1;
}
UserList::UserList()
{
    userlist = new Bag<User>();
}
UserList::~UserList()
{
    delete userlist;
}
BagIterator<User> UserList::begin()
{
    return userlist->begin();
}
BagIterator<User> UserList::end()
{
    return userlist->end();
}
void UserList::addUser(std::string user_name, std::string pass_word, std::string home_town)
{
    User newUser(user_name, pass_word, home_town);
    userlist->push_back(newUser);
}
void UserList::addUser(const User &user)
{
    userlist->push_back(user);
}
void UserList::deleteUser(std::string username)
{
    User dummy(username, "", "");
    userlist->remove(dummy);
}
void UserList::socialnetwork(User &user)
{
    int choice = 0;
    std::cout << "Would you like to:\n1. Display all your existing wall posts.\n2. Create a new wall post.\n3. Delete an existing wall post by number.\n4. Change your password or hometown.\n5. Search for an existing user to send a friend request to.\n6. Look at all pending friend requests.\n7. See all friends and delete friends.\n8. Log out.\n";
    std::cin >> choice;
    std::cin.ignore();
    if(std::cin)
    {
        switch(choice)
        {
            case 1:
            {
                user.printWall();
                socialnetwork(user);
                break;
            }
            case 2:
            {
                std::string postbody = "";
                std::cout << "Please enter the body of the wall post you want to write.\n";
                getline(std::cin, postbody);
                user.writePost(postbody);
                socialnetwork(user);
                break;
            }
            case 3:
            {
                int numOfPost = -1;
                std::cout << "Please enter the number of the wall post you would like to delete.\n";
                std::cin >> numOfPost;
                user.deletePost(numOfPost);
                socialnetwork(user);
                break;
            }
            case 4:
            {
                int cosa = 0;
                std::string newHometown = "";
                std::string newPassword = "";
                std::cout << "Would you like to change your: 1. password 2. hometown?\n";
                std::cin >> cosa;
                if(cosa == 1)
                {
                    std::cout << "Please enter a new password:\n";
                    std::cin >> newPassword;
                    user.setPassword(newPassword);
                    socialnetwork(user);
                }
                else if(cosa == 2)
                {
                    
                    std::cout << "Please enter a new hometown:\n";
                    std::cin >> newHometown;
                    user.setHometown(newHometown);
                    socialnetwork(user);
                }
                else
                {
                    std::cout << "Invalid input detected. Please choose again.\n";
                    socialnetwork(user);
                }
                break;
            }
            case 5:
            {
                std::string username_substring = "";
                std::cout << "Please enter at least part of a username to search for users." << std::endl;
                std::cin >> username_substring;
                std::transform(username_substring.begin(), username_substring.end(), username_substring.begin(), ::tolower);
                int matchCount = 0;
                User **userArray = new User*[userlist->size()];
                for(BagIterator<User> it = userlist->begin(); it!=userlist->end(); it++)
                {
                    std::string username = (*it).getUsername();
                    if(username!=user.getUsername()) // so they don't find themselves on search
                    {
                    std::transform(username.begin(), username.end(), username.begin(), ::tolower);
                    if(username.find(username_substring)!=std::string::npos)
                    {
                        userArray[matchCount] = &(*it);
                        matchCount++;
                    }
                    }
                    
                }
                std::cout << "Please select which user to send a friend request to. Type in the number next to their name to send a friend request to them:" << std::endl;
                int friendChoice = 0;
                for(int i = 0; i < matchCount; i++)
                {
                    std::cout << i+1 << ". " << userArray[i]->getUsername() << std::endl;
                }
                if(matchCount) {
                std::cin >> friendChoice;
                friendChoice = friendChoice - 1; //user will type in '1' for '0'
                user.sendFriendRequest(*userArray[friendChoice]);
                }
                else
                {
                    std::cout << "No friends found." << std::endl;
                }
                delete []userArray;
                socialnetwork(user);
                break;
                
            }
            case 6:
            {
                std::string friendString = "";
                bool yesOrNo = false;
                std::cout << "Here are all pending friend requests: "<< std::endl;
                user.printPending(true);
                std::cout << "Please type in the username of the person followed by a ',' followed by either 'yes' to add them to your friends list or 'del' to delete the pending request (i.e. Steven Francus,yes) or any key to quit." << std::endl;
                std::cin.clear();
                std::getline(std::cin, friendString);
                int nexi = firstIndexOfChar(friendString, ',');
                std::string username = friendString.substr(0, nexi);
                std::string answer = friendString.substr(nexi+1, friendString.length());
                std::transform(answer.begin(), answer.end(), answer.begin(), ::tolower);
                if(answer=="yes") { yesOrNo = true; }
                else if(answer=="del") { yesOrNo = false; }
                else { break; }
                User *userd;
                for(BagIterator<User> it = userlist->begin(); it!=userlist->end(); it++)
                {
                    std::string usern = (*it).getUsername();
                    if(usern==username) // so they don't find themselves on search
                    {
                            userd = &(*it);
                            break;
                    }
                }
                user.respondToFriendRequest(*userd, yesOrNo);
                socialnetwork(user);
                break;
            }
            case 7:
            {
                std::string delString = "";
                std::cout << "Here are all your friends: " << std::endl;
                user.printFriends(true);
                std::cout << "To delete a friend, please enter their username below. To exit, please type in 'q'." << std::endl;
                std::getline(std::cin, delString);
                if(delString=="q") { break; }
                User *userd;
                for(BagIterator<User> it = userlist->begin(); it!=userlist->end(); it++)
                {
                    std::string usern = (*it).getUsername();
                    if(usern==delString) // so they don't find themselves on search
                    {
                        userd = &(*it);
                        break;
                    }
                }
                    try {
                        user.removeFriend(*userd);
                    }
                    catch(std::exception &e)
                    {
                        std::cout << e.what() << std::endl;
                    }
                socialnetwork(user);
                break;
            }
            case 8:
            {
                std::cin.clear();
                return;
            }
            default:
            {
                std::cout << "Invalid input detected. Please choose again.\n";
                socialnetwork(user);
            }
        }
    }
}
void UserList::writeListToFile(std::string filename)
{
    std::ofstream file;
    file.open(filename.c_str());
    for(BagIterator<User> mu = userlist->begin(); mu != userlist->end(); mu++)
    {
        file << (*mu).userDataToString() + (*mu).writeWallToFile() + "^";
    }
    file << "+";
    for(BagIterator<User> mu = userlist->begin(); mu != userlist->end(); mu++)
    {
        file << (*mu).printFriends(false) + (*mu).printPending(false);
    }
    file.close();
}
void UserList::readListFromFile(std::string filename)
//shit gets weird here yo
{
    std::ifstream aus;
    aus.open(filename.c_str(), std::ios::in);
    std::string friendOr = "";
    std::string linea = "";
    while(aus.good())
    {
        getline(aus, linea, '^');
        if(linea[0]=='%')
        {
            std::string userdata, username, password, hometown;
            linea = linea.substr(1, linea.length()-1);//strips off delimiters & and %
            int index = firstIndexOfChar(linea, '\n');
                //splits off first line
            userdata = linea.substr(0, index);
            linea = linea.substr(index+1, linea.length()); //contains pending friends,friends,and wall posts
            int username_index = firstIndexOfChar(userdata, '.');
            
            username = userdata.substr(0, username_index);
            userdata = userdata.substr(username_index+1, userdata.length()-1);
            
            password = userdata.substr(0, firstIndexOfChar(userdata, '.'));
            userdata = userdata.substr(firstIndexOfChar(userdata, '.'), userdata.length()-1);
            
            hometown = userdata.substr(1, userdata.length()-1);
            User user(username, password, hometown);
            std::string wallpost = "";
            std::string pendingFriend = "";
            std::string friendName = "";
            std::stringstream ss(linea);
            while(std::getline(ss, wallpost, '*'))
            {
                int index = firstIndexOfChar(wallpost, '|');
                wallpost = wallpost.substr(index+1, wallpost.length()-1);
                if(wallpost.length())
                    user.writePost(wallpost);
            }
            addUser(user);
        }
           }
   aus.close();
    std::ifstream aut;
    aut.open(filename.c_str(), std::ios::in);
    while(aut.good())
    {
        while(std::getline(aut, friendOr, '+'))
        {
            if(friendOr[0]!='%')
            {
                std::string buffer = "";
                std::stringstream rr(friendOr);
                while(std::getline(rr,buffer))
                {
                    int checkPend = firstIndexOfChar(buffer,'>');
                    int checkFriend = firstIndexOfChar(buffer,':');
                    if(checkPend==-1)
                    {
                        std::string userOwn = buffer.substr(0, checkFriend);
                        std::string userAdd = buffer.substr(checkFriend+1, buffer.length()-1);
                        User *userOwne, *userAdde;
                        for(BagIterator<User> it = userlist->begin(); it!=userlist->end(); it++)
                        {
                            std::string usern = (*it).getUsername();
                            if(usern==userOwn) // so they don't find themselves on search
                            {
                                userOwne = &(*it);
                            }
                            if(usern==userAdd)
                            {
                                userAdde = &(*it);
                            }
                        }
                        userOwne->addFriend(*userAdde);
                    }
                    else if(checkFriend==-1)
                    {
                        std::string userOwn = buffer.substr(0, checkPend);
                        std::string userAdd = buffer.substr(checkPend+1, buffer.length()-1);
                        User *userOwne, *userAdde;
                        for(BagIterator<User> it = userlist->begin(); it!=userlist->end(); it++)
                        {
                            std::string usern = (*it).getUsername();
                            if(usern==userOwn) // so they don't find themselves on search
                            {
                                userOwne = &(*it);
                            }
                            else if(usern==userAdd)
                            {
                                userAdde = &(*it);
                            }
                        }
                        userOwne->addPending(*userAdde);
                        
                    }
                }
            }
        }
    }
}//exactly 100 lines huehuehue
bool UserList::findUser(std::string user_name, User &assignee)
{
    User *emptyUser = new User(user_name, "", "");
    for(BagIterator<User> mu = userlist->begin(); mu!=userlist->end(); mu++)
    {
        if((*mu)==*emptyUser)
        {
            assignee = *mu;
            return true;
        }
    }
    return false;
}
