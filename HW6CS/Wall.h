//
//  Wall.h
//  LinkedListTemplate
//
//  Created by Steven Francus on 9/21/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef LinkedListTemplate_Wall_h
#define LinkedListTemplate_Wall_h
#include "LinkedList.h"
#include "LinkedListImp.cpp"
#include "WallPost.h"
#include <sstream>
#include <fstream>
#include <ostream>
class Wall
{
public:
    Wall();
    ~Wall();
    void addPost(const WallPost &post);
    void removePost(int number);
    std::string WallToString(bool readable);
    void StringToWall(std::string wallstring);
    void readFromFile(std::string filename);  
    int length() const;
    
private:
    List<WallPost*>* wallOf;
};
#endif
