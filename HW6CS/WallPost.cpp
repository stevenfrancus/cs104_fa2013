#include <iostream>
#include "WallPost.h"

WallPost::WallPost(std::string authorName, std::string bodyMsg) : body(bodyMsg), authorname(authorName)
{
}
WallPost::WallPost() : body(""), authorname("")
{
}
WallPost::~WallPost()
{
}
void WallPost::setBody(std::string message)
{
	body = message;
}
std::string WallPost::getBody() const
{
	return body;
}
void WallPost::setAuthor(std::string authorNm)
{
	authorname = authorNm;
}
std::string WallPost::getAuthor() const
{
	return authorname;
}
int WallPost::getPostNumber() const
{
	return postNumber;
}
void WallPost::setPostNumber(int post)
{
	postNumber = post;
}
void WallPost::printWallPost() const
{
    std::cout << "Post number: #" << postNumber;
    std::cout << std::endl;
	std::cout << "Post author name: " << authorname;
	std::cout << std::endl;
	std::cout << "Post body: " << body;
	std::cout << std::endl;
}
std::string WallPost::formatWallPost() const
{
    std::string post = "";
    post = "*" + authorname + "|" + body;
    return post;
}