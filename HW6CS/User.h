//
//  User.h
//  LinkedListTemplate
//
//  Created by Steven Francus on 9/21/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef LinkedListTemplate_User_h
#define LinkedListTemplate_User_h
#include "Wall.h"
#include "linkedlisttemplate.h"
#include "linkedlisttemplateImp.cpp"
#include <exception>
class User 
{
public:
    User();
    User(std::string userstring);
    User(std::string user_name, std::string pass_word);
    User(std::string user_name, std::string pass_word, std::string home_town);
    void setHometown(std::string);
    std::string getHometown() const;
    std::string getUsername() const;
    std::string getPassword() const;
    void setPassword(std::string);
    void writePost(std::string);
    void deletePost(int number);
    void outputUserData() const;
    void printWall();
    void addFriend(const User&);
    void removeFriend(User&); 
    void addPending(const User&);
    void sendFriendRequest(User&);
    void respondToFriendRequest(User&, bool repons);
    std::string printPending(bool);
    std::string printFriends(bool);
    std::string userDataToString() const;
    std::string writeWallToFile();
    ~User();
    bool operator==(User const & other);
private:
    Wall wall;
    ADTBag<User>* friends;
    ADTBag<User>* pendingFriends;
    std::string username;
    std::string password;
    std::string hometown;
    
};

#endif
