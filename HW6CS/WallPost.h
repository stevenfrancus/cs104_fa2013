#include <iostream>
#ifndef LinkedListTemplate_wallpost_h
#define LinkedListTemplate_wallpost_h

class WallPost
{
	public:
	WallPost(std::string a, std::string b);
	WallPost();
	~WallPost();
	void setBody(std::string);
	std::string getBody() const;
	void setAuthor(std::string);
	std::string getAuthor() const;
	void printWallPost() const;
    std::string formatWallPost() const;
	int getPostNumber() const;
	void setPostNumber(int);

	private:
	std::string body;
	std::string authorname;
	int postNumber;
};
#endif