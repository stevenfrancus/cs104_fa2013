//
//  UserList.h
//  LinkedListTemplate
//
//  Created by Steven Francus on 9/21/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef LinkedListTemplate_UserList_h
#define LinkedListTemplate_UserList_h
#include "User.h"
#include "Node.h"
#include <fstream>
#include <iostream>
#include <istream>
#include <sstream>
#include "linkedlisttemplate.h"
class UserList
{
public:
    UserList();
    ~UserList();
    void socialnetwork(User& user);
    void addUser(const User &user);
    void addUser(std::string user_name, std::string pass_word, std::string home_town);
    void deleteUser(std::string user);
    void writeListToFile(std::string filename);
    void readListFromFile(std::string filename);
    bool findUser(std::string username, User &assignee);
    BagIterator<User> begin();
    BagIterator<User> end();
private:
    ADTBag<User>* userlist;
};

#endif
