//
//  linkedlisttemplate.h
//  LinkedListTemplate
//
//  Created by Steven Francus on 9/19/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef LinkedListTemplate_linkedlisttemplate_h
#define LinkedListTemplate_linkedlisttemplate_h

#include "Node.h"
#include "ADTBag.h"
#include "BagIterator.h"
#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <string>
template<class T> class Node;
template<class T>
class Bag : public ADTBag<T>
{
public:
    Bag();
    Bag(Node<T>*);
    virtual ~Bag();
    virtual void push_back(const T &);
    virtual void remove(T &);
    virtual bool contains(const T&);
    virtual int size();
    virtual BagIterator<T> begin();
    virtual BagIterator<T> end();
private:
    Node<T> *head;
    Node<T> *tail;
    int sizeOf;
};
#endif
