//
//  ADTBag.h
//  
//
//  Created by Steven Francus on 10/28/13.
//
//

#ifndef _ADTBag_h
#define _ADTBag_h
#include "BagIterator.h"
#include "BagIteratorImp.cpp"
#include "Node.h"
template <class T>
class ADTBag
{
public:
    virtual ~ADTBag();
    virtual void push_back(const T &) = 0;
    virtual void remove(T &) = 0;
    virtual bool contains(const T&) = 0;
    virtual int size() = 0;
    virtual BagIterator<T> begin() = 0;
    virtual BagIterator<T> end() = 0;
};
template <class T>
ADTBag<T>::~ADTBag()
{
    
}


#endif
