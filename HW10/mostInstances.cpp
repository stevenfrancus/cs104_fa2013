//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include "HashTable.h"
#include "HashTableImp.cpp"
#include "genericString.h"
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
using namespace std;
int main(int argc, const char * argv[])
{
    std::ifstream aus;
    aus.open("itemDuplicates.txt", std::ios::in);
    int counter = 0;
    int length = 0;
    std::string firstLen = "";
    std::getline(aus, firstLen);
    std::stringstream str(firstLen);
    str >> length;
    Hashtable<genericString, int> roster(length);
    genericString *stringArray = new genericString[length];
    while(aus.good())
    {
        std::string len = "";
        std::getline(aus, len);
            genericString buddy(len);
        int pen = 0;
        try {
            pen = roster.get(buddy); 
        }
        catch(exception &e)
        {
        }
           roster.add(buddy, ++pen);
            stringArray[counter] = buddy;
        counter++;
    }
    int _m = 0;
    for(int i = 0; i < length; i++)
    {
        int pen = 0;
        try {
            pen = roster.get(stringArray[i]);
        }
        catch(exception &e) { }
        _m = std::max(pen, _m);
    }
    if(_m > 1)
    {
    cout << "The most duplicated item(s) (the items to donate one of): " << endl;
    for(int i = 0; i < length; i++)
    {
        int pen = 0;
        try
        {
            pen = roster.get(stringArray[i]);
        }
        catch(exception &e) { }
        if(pen==_m)
        {
            try {
            std::cout << stringArray[i].strung << std::endl;
            roster.remove(stringArray[i]);
            }
            catch(std::exception &e)
            { }
        }
    }
    }
    else
    {
        cout << "No item occurs more than once so no item should be given away." << endl;
    }
    delete []stringArray;
    return 0;
}
