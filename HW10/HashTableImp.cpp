//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include "HashTable.h"
template <class KeyType, class ValueType>
Hashtable<KeyType,ValueType>::Hashtable(int initialSize)
{
    this->resize(initialSize);
}
template <class KeyType, class ValueType>
void Hashtable<KeyType,ValueType>::add(const KeyType& key, const ValueType& value)
{
    if(_size/this->capacity() >= .5)
    {
        resizeHash();
    }
    int index = key.hash(this->capacity());
    Node<KeyType,ValueType> nodulus;
    nodulus.key = key;
    nodulus.value = value;
    for(int i = 0; i < (*this)[index].size(); i++)
    {
        if((*this)[index][i].key == key)
        {
            (*this)[index][i].value = value;
            return;
        }
    }
    (*this)[index].push_back(nodulus);
    _size++;
}
template <class KeyType, class ValueType>
void Hashtable<KeyType,ValueType>::remove(const KeyType& key)
{
    KeyType emptyValue;
    int index = key.hash(this->capacity());
    if((*this)[index].size()==0) { throw std::logic_error("Item not found"); }
    for(int i = 0; i < (*this)[index].size(); i++)
    {
        if((*this)[index][i].key == key)
        {
            (*this)[index].erase((*this)[index].begin()+i);
        }
    }
    if((*this)[index].size()==0) { _size--; }
}
template <class KeyType, class ValueType>
ValueType & Hashtable<KeyType,ValueType>::get(const KeyType &key)
{
    int index = key.hash(this->capacity());
    for(int i = 0; i < (*this)[index].size(); i++)
    {
        if((*this)[index][i].key == key)
        {
            return (*this)[index][i].value;
        }
    }
    throw std::logic_error("Item not found");
}
template <class KeyType, class ValueType>
void Hashtable<KeyType,ValueType>::resizeHash()
{
    int size = primeFinder();
    Hashtable _table(size);
    for(int i = 0; i < this->size(); i++)
    {
        for(int j = 0; j < (*this)[i].size(); i++)
        {
            _table.add((*this)[i][j].key, (*this)[i][j].value);
        }
    }
    *this = _table;
}
template <class KeyType, class ValueType>
int Hashtable<KeyType, ValueType>::primeFinder() //uses Bertrand's postulate to approximate a new table value around 2*current table size - 2
{
    int prime = 97;
    if(this->capacity() < 53)
    {
        return 97;
    }
    for(int i = this->capacity()*2-2; i > this->capacity(); i = i - 2)
        //since the size is assumed to be prime we can conclusively tell that size mod 2 is 1, so the next number is even. We can improve algorithmic efficiency by skipping each even number
    {
        bool isPrime = true;
        for(int j = 2; j < sqrt(i); j++)
        {
            if(i%j==0) isPrime = false;
        }
        if(isPrime) {
            prime = i;
            break;
        }
    }
    return prime;
    //we can approximately that as the capacity of the vector goes to infinity, the prime number theorem states that the chances of a number being prime is approximately 1/ln(n) where n is the number. Then, to find if a number is prime the time it takes is approximately O(sqrt(n)). Therefore we can surmise that finding a prime number will take approximately O(ln(2n)*n^(1/2)) = O(n^(1/2)log(n))
}