//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//
#ifndef gen_H
#define gen_H
#include <vector>
#include <iostream>
#include <stdio.h>
#include <cmath>
#include <stdexcept>
class genericString
{
public:
    genericString(std::string n);
    genericString();
    std::string strung;
    bool operator==(const genericString & other) const;
    bool operator!=(const genericString &other) const;
    genericString& operator=(const genericString& p);
    int hash(int _size) const; //modified djb2 hash
};

#endif
