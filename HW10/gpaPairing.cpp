//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include "HashTable.h"
#include "HashTableImp.cpp"
#include "genericString.h"
#include <string>
#include <iostream>
#include <sstream>
int main(int argc, const char * argv[])
{
    Hashtable<genericString, double> roster(37);
    while(true)
    {
        std::cout << "Would you like to:\n1. add a student record\n2. remove a student record \n3. Look up a student\n4. exit?\n";
        int choice = 0;
        std::cin >> choice;
        std::cin.ignore();
        if(choice == 1)
        {
            double gpa = 0.0;
            std::string studentNam, studentI;
            std::cout << "What is the student's name?" << std::endl;
            std::getline(std::cin, studentNam);
            std::cout << "What is the student's GPA?" << std::endl;
            std::getline(std::cin, studentI);
            std::stringstream str(studentI);
            str >> gpa;
            genericString newStudent(studentNam);
            try{
               roster.add(newStudent, gpa);
            }
            catch(std::exception &e) { std::cout << e.what() << std::endl; }
            
        }
        else if(choice == 2)
        {
            std::string studentN = "";
            std::cout << "What is the student's name?" << std::endl;
            std::getline(std::cin, studentN);
            genericString deleteStudent(studentN);
            try {
                roster.remove(deleteStudent);
            }
            catch(std::exception &e)
            {
                std::cout << e.what() << std::endl;
            }
        }
        else if(choice == 3)
        {
            bool failed = false;
            std::string studentN = "";
            std::cout << "What is the student's name?" << std::endl;
            std::getline(std::cin, studentN);
            genericString Student(studentN);
            double gpa = 0.0;
            try {
                gpa = roster.get(Student);
            }
            catch(std::exception &e)
            {
                failed = true;
                std::cout << e.what() << std::endl;
            }
            if(!failed)
                std::cout << studentN << "'s GPA is: " << gpa << std::endl;
        }
        else
        {
            break;
        }
    }
    return 0;
}
