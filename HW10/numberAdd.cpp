//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include "HashTable.h"
#include "HashTableImp.cpp"
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
using namespace std;
class numHash
{
public:
    numHash(int n) : number(n) {};
    numHash() : number(0) {};
    int number;
    numHash& operator=(const numHash& p)
    {
        this->number = p.number;
    };
    bool operator==(const numHash& p) const
    {
        this->number == p.number;
    };
    int hash(int _size) const //modified djb2 hash
    {
        return number%_size;
    };

};
int main(int argc, const char * argv[])
{
    std::ifstream aus;
    aus.open("calorieCount.txt", std::ios::in);
    int counter = 0;
    int count = 0;
    int n, m, total, buffer;
    std::string firstLen = "";
    std::getline(aus, firstLen);
    std::stringstream str(firstLen);
    while(str >> buffer)
    {
        if(count == 0)
        {
        n = buffer;
        }
        else if(count == 1)
        {
            m = buffer;
        }
        else
        {
            total = buffer;
        }
        count++;
    }
    Hashtable<numHash, int> turkey(n);
    int *array = new int[n];
    Hashtable<numHash, int> sides(m);
    while(aus.good())
    {
        std::string len = "";
        std::getline(aus, len);
        int val = 0;
        std::stringstream el(len);
        el >> val;
        numHash hash(val);
        if(counter < n)
        {
        turkey.add(hash, val);
            array[counter] = val;
        }
        else if(counter < n+m)
        {
        sides.add(hash, val);
        }
        counter++;
    }
    for(int i = 0; i < n; i++)
    {
        int pen = 0;
        numHash _try(array[i]);
        try {
            pen = turkey.get(_try);
        }
        catch(exception &e) { }
        int penm = 0;
        numHash _tryA(total - pen);
        try {
            penm = sides.get(_tryA);
        }
        catch(exception &e) { }
        if(pen > 0 && penm > 0)
        {
            turkey.remove(_try);
            sides.remove(_tryA);
        cout << total << " = " << pen << " + " << penm << endl;
        }
    }
    delete []array;
    return 0;
}
