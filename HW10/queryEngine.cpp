//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include "HashTable.h"
#include "HashTableImp.cpp"
#include "genericString.h"
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
using namespace std;

void breakInsert(Hashtable<genericString,vector<int> > &map, string strang, int count)
{
    string len = "";
    stringstream str(strang);
    while(str >> len)
    {
        genericString genera(len);
        vector<int> vec;
        try {
            vec = map.get(genera);
        }
        catch(exception &e)
        {
            
        }
        vec.push_back(count);
        map.add(genera, vec);
    }
}

int main(int argc, const char * argv[])
{
    std::ifstream aus;
    aus.open("queries.txt", std::ios::in);
    int length = 0;
    int check = 0;
    string firstLen = "";
    getline(aus, firstLen);
    stringstream str(firstLen);
    str >> length;
    Hashtable<genericString, vector<int> > pageList(length);
    vector<string> pages;
    while(aus.good())
    { 
        string len = "";
        getline(aus, len);
        breakInsert(pageList, len, check);
        pages.push_back(len);
        check++;
    }
    cout << "Please enter a query word to bring up all matching pages: " << endl;
    cin >> firstLen;
    genericString gen(firstLen);
    vector<int> vec;
    try {
    vec = pageList.get(gen);
    }
    catch(exception &e)
    {
        cout << e.what() << endl;
    } 
    for(int i = 0; i < vec.size(); i++)
    {
        cout << pages[vec[i]] << endl;
    }
    return 0;
}
