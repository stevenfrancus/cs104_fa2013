//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//
#ifndef STUDENT_H
#define STUDENT_H
#include <vector>
#include <iostream>
#include <stdio.h>
#include <cmath>
//#include "AbstractList.h"
#include <stdexcept>
template <class KeyType, class ValueType>
struct Node {
    KeyType key;
    ValueType value;
};
template <class KeyType, class ValueType>
class Hashtable : protected std::vector< std::vector< Node<KeyType,ValueType> > > {
public:
    void add (const KeyType & key, const ValueType & value);
    void remove (const KeyType & key);
    ValueType & get (const KeyType & key);
    Hashtable (int initialSize);
private:
    void resizeHash();
    int primeFinder();
    int _size;
};
#endif
