//
//  main.cpp
//  LinkedListTemplate
//
//  Created by Steven Francus on 9/19/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include <iostream>
#include "node.h"
#include "Wall.h"
#include "User.h"
#include "UserList.h"
#include <cstdlib>
#include <ctime>
#include <fstream>
using namespace std;

void generateRandomList(UserList* userl, int numUsers, int numWallPosts, int numFriends)
{
    for(int i = 0; i < numUsers; i++)
    {
        stringstream ra;
        string nom = "";
        ra << i;
        nom = ra.str();
        User usern(nom, nom, nom);
        for(int j = 0; j < numWallPosts; j++)
        {
            stringstream re;
            string nome = "";
            re << j;
            nome = re.str();
            usern.writePost(nome);
        }
        userl->addUser(usern);
    }
    srand(time(NULL));
    for(BagIterator<User> mu = userl->begin(); mu != userl->end(); mu++)
    {
        
        int ran = rand() % numUsers;
        int counter = 0;
        int friendCount = 0;
        for(BagIterator<User> me = userl->begin(); me != userl->end(); me++)
        {
            if(counter >= ran && friendCount < numFriends)
            {
                (*mu).addFriend((*me));
                (*me).addFriend((*mu));
                friendCount++;
            }
            counter ++;
        }
    }

}
int main()
{
   int choice = 0;
    UserList* userlist = new UserList();
    int first_choice = 0;
    string file_dump = "";
    std::cout << "Welcome to the Social Network! Would you like to:\n1. Generate 10,000 users with 100 friends each and 10 wall posts\n2. Run the program directly?" << std::endl;
    cin >> first_choice;
    cin.ignore();
    if(first_choice == 1)
    {
        generateRandomList(userlist, 10000, 10, 100);
        file_dump = "test.txt";
    }
    else
    {
    userlist->readListFromFile("example.txt");
        file_dump = "example.txt";
    }
    bool endtimescenario = false;
    while(!endtimescenario)
    {
    cout << "Welcome to the Social Network beta! \n Would you like to: \n 1) Log in as a user? \n 2) Create a new user? \n 3) Quit the program?\n";
    cin >> choice;
        cin.ignore();
    if(cin)
    {
        switch(choice)
        {
            case 1:
            {
                string username = "";
                string password = "";
                bool loggedin = false;
                cout << "You selected: log in as a user. To log in as a user, please enter your username now.\n";
                getline(cin, username);
                for(BagIterator<User> mu = userlist->begin(); mu != userlist->end(); mu++)
                {
                    if((*mu).getUsername()==username)
                    {
                        cout << "Please enter your password now.\n";
                        getline(cin, password);
                        if((*mu).getPassword()==password)
                        {
                            loggedin = true;
                            userlist->socialnetwork((*mu));
                            userlist->writeListToFile(file_dump.c_str());
                        }
                    }
                }
                if(!loggedin)
                {
                    cout << "Log in failed. Maybe your password or username was not correct.\n";
                }
                break;
            }
            case 2:
            {
                User user;
                string username, password, hometown;
                cout << "You selected: create new user. To create a new user, please enter a username to assign the new user." << endl;
                getline(cin, username);
                cin.clear();
                if(userlist->findUser(username, user))
                {
                    cout << "This username is already taken. Please enter a new username.\n";
                    getline(cin, username);
                    cin.ignore();
                }
                cout << "Please assign a password for the new user." << endl;
                getline(cin, password);
                cin.clear();
                cout << "Please give a hometown for the new user. You may leave this blank if you wish.\n";
                getline(cin, hometown);
                User newUser(username, password, hometown);
                userlist->addUser(newUser);
                userlist->writeListToFile(file_dump.c_str());
                break;
            }
            case 3:
            {
                endtimescenario = true;
                break;
            }
            default:
            {
                cout << "Input is incorrect." << endl;
                endtimescenario = true;
                break;
            }
        }
    }
    else
    {
        
    }
    }
}


