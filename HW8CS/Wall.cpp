#include "Wall.h"
//#include "WallPost.h"
#include "Node.h"
#include <sstream>
#include <fstream>
#include <ostream>
Wall::Wall()
{
    wallOf = new LinkedList<WallPost*>();
}
Wall::~Wall()
{
}
int Wall::length() const
{
    return wallOf->length();
}
void Wall::addPost(const WallPost &postem)
{
    WallPost *post = new WallPost(postem.getAuthor(), postem.getBody(), postem.getTimeCreated());
    post->setPostNumber(wallOf->length()+1);
    wallOf->insert(wallOf->length(), post);
}
int Wall::partition(bool(*compare)(WallPost&, WallPost&), int left_bound, int right_bound)
{
    int i = left_bound;
    for(int j = left_bound; j < right_bound; j++)
    {
        if(compare(*wallOf->get(j), *wallOf->get(right_bound)))
        {
            WallPost* walter = wallOf->get(i);
            WallPost* otherwalter = wallOf->get(j);
            walter->setPostNumber(j+1);
            otherwalter->setPostNumber(i+1);
            wallOf->set(i, otherwalter);
            wallOf->set(j, walter);
            i++;
        }
    }
    WallPost* walter = wallOf->get(i);
    WallPost* waltern = wallOf->get(right_bound);
    walter->setPostNumber(right_bound+1);
    waltern->setPostNumber(i+1);
    wallOf->set(i, waltern);
    wallOf->set(right_bound, walter);
    return i;
}
void Wall::quickSort(bool(*compare)(WallPost&, WallPost&), int left_bound, int right_bound)
{
    if(left_bound < right_bound)
    {
        int p = partition(compare, left_bound, right_bound);
        quickSort(compare, left_bound, p-1);
        quickSort(compare, p+1, right_bound);
    }
}

WallPost* Wall::getPost(int pos)
{
    WallPost *mew;
    int count = 0;
    for(ListIterator<WallPost*> mu = wallOf->begin(); mu!=wallOf->end(); mu++)
    {
        if(count==pos)
        {
            mew = (*mu);
        }
        count++;
    }
    if(mew == NULL) { throw std::logic_error("Position out of bounds"); }
    return mew;
}
void Wall::removePost(std::string username, int number)
{
    try {
        int count = 0;
        for(ListIterator<WallPost*> mu = wallOf->begin(); mu!=wallOf->end(); mu++)
        {
            if((*mu)->getAuthor()==username && count==(number-1))
            {
            wallOf->remove(number-1);
            }
            count++;
        }
    }
    catch(std::exception &e)
    {
        std::cout << e.what() << std::endl;
    }
}
void Wall::removePost(int number)
{
    try {
        wallOf->remove(number-1);
    }
    catch(std::exception &e)
    {
        std::cout << e.what() << std::endl;
    }
}
std::string Wall::WallToString(bool readable)
{
    std::string varchar = "";
    for(ListIterator<WallPost*> mu = wallOf->begin(); mu!=wallOf->end(); mu++)
    {
        if(readable)
        {
        (*mu)->printWallPost();
        }
         else
        {
        varchar += (*mu)->formatWallPost();
        }
    }
    return varchar;
}
void Wall::StringToWall(std::string wallstring)
{
    WallPost *append = new WallPost();
    std::stringstream re(wallstring);
    std::string pushback;
    int i = 0;
    while(std::getline(re, pushback, '|'))
    {
        if(i==0) //how I get the post author
        {
            append->setAuthor(pushback);
        }
        else if(i==1) //how I get the post body
        {
            append->setBody(pushback);
        }
        else if(i==2) //how I get the time created
        {
            int result = 0;
            std::stringstream ss(pushback);
            ss >> result;
            append->setTimeCreated(result);
        }
        else if(i==3) //how I get the comments out
        {
            std::string wallpostform = "";
            std::stringstream ra(pushback);
            while(std::getline(ra, wallpostform, '>'))
                  {
                      int counter = 0;
                      std::string pushforward = "";
                      std::stringstream ro(wallpostform);
                      wallpostComment com;
                      while(std::getline(ro, pushforward, '`'))
                      {
                          if(counter==0)
                          {
                              com.setCommentBody(pushforward);
                          }
                          else if(counter==1)
                          {
                              com.setPoster(pushforward);
                          }
                          else if(counter==2)
                          {
                              int result = 0;
                              std::stringstream ss(pushforward);
                              ss >> result;
                              com.setTimeCreated(result);
                          }
                          else
                          {
                              
                          }
                          counter++;
                      }
                      append->addComment(com);
                  }
        }
        i++;
    }
    append->setPostNumber(wallOf->length()+1);
    wallOf->insert(wallOf->length(), append);
}
void Wall::readFromFile(std::string filename)
{
    std::ifstream db;
    db.open(filename.c_str(), std::ios::in);
    if(!db) {
        std::cerr << "File could not be opened." << std::endl;
        std::exit(1);
    }
    std::string ks = "";
    while(!db.eof()) {
        getline(db, ks);
        StringToWall(ks);
    }
}
