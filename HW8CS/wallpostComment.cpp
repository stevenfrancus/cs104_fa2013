//
//  linkedlisttemplateImp.cpp
//  
//
//  Created by Steven Francus on 10/28/13.
//
//

#include "wallpostComment.h"
wallpostComment::wallpostComment(std::string author, std::string body) : username(author), commentBody(body), time(0)
{
}
wallpostComment::wallpostComment() : username(""), commentBody(""), time(0)
{
    
}
std::string wallpostComment::getCommentBody() const
{
    return commentBody;
}
void wallpostComment::setCommentBody(std::string comment)
{
    commentBody = comment;
}
std::string wallpostComment::getPoster() const
{
    return username;
}
void wallpostComment::setPoster(std::string poster)
{
    username = poster;
}
int wallpostComment::getTimeCreated() const
{
    return time;
}
void wallpostComment::setTimeCreated(int newtime)
{
    time = newtime;
}