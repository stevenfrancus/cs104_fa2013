//
//  node.h
//  LinkedListTemplate
//
//  Created by Steven Francus on 9/19/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef LinkedListTemplate_node_h
#define LinkedListTemplate_node_h
template <class T> class List;
template<class T>
class Node
{
public:
    Node* getNext();
    void setNext(Node*);
    Node* getPrev();
    void setPrev(Node*);
    T& getData();
    void setData(const T&);
private:
    T data;
    Node *prev;
    Node *next;
};
template <class T>
Node<T>* Node<T>::getNext()
{
    return next;
};
template <class T>
Node<T>* Node<T>::getPrev()
{
    return prev;
};
template <class T>
void Node<T>::setPrev(Node<T>* n)
{
    prev = n;
};
template <class T>
void Node<T>::setNext(Node<T>* m)
{
    next = m;
};
template <class T>
void Node<T>::setData(const T& a)
{
    data = a;
};
template <class T>
T& Node<T>::getData()
{
    return data;
}

#endif
