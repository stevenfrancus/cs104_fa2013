//
//  LinkedList.h
//  HW4CS
//
//  Created by Steven Francus on 9/29/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef HW4CS_LinkedList_h
#define HW4CS_LinkedList_h
#include "ListIterator.h"
#include "AbstractList.h"
#include <stdexcept>
template<class T> class Node;
template <class T>
class LinkedList : public List<T>
{
public:
    LinkedList();
    LinkedList(Node<T>* head);
    virtual ~LinkedList();
    virtual void insert (int pos, const T & item) throw(std::exception);
    virtual int length() const;
    virtual void remove (int pos) throw(std::exception);
    virtual void set (int pos, const T & item) throw(std::exception);
    virtual T const & get (int pos) const throw(std::exception);
    virtual ListIterator<T> begin();
    virtual ListIterator<T> end();
private:
    Node<T> *head;
    Node<T> *tail;
    int lengthOf;
};
#endif