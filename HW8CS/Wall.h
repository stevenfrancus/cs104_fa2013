//
//  Wall.h
//  LinkedListTemplate
//
//  Created by Steven Francus on 9/21/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef LinkedListTemplate_Wall_h
#define LinkedListTemplate_Wall_h
//#include "LinkedList.h"
//#include "LinkedListImp.cpp"
#include "WallPost.h"
#include <sstream>
#include <fstream>
#include <ostream>
class Wall
{
public:
    Wall();
    ~Wall();
    void addPost(const WallPost &post);
    WallPost* getPost(int);
    void removePost(int number);
    void removePost(std::string username, int number);
    std::string WallToString(bool readable);
    void StringToWall(std::string wallstring);
    void readFromFile(std::string filename);
    void quickSort(bool(*foo)(WallPost&, WallPost&),int, int);
    int length() const;
    
private:
    List<WallPost*>* wallOf;
    int partition(bool(*foo)(WallPost&, WallPost&), int, int);
};
#endif
