#include "User.h"
User::User() : username(""), password(""), hometown("")
{
    friends = new Bag<User>();
    pendingFriends = new Bag<User>();
}
User::User(std::string userstring)
{
    std::stringstream re(userstring);
    std::string pushback;
    int i = 0;
    while(std::getline(re, pushback, '.'))
    {
        if(i==0)
            username = pushback;
        else if(i==1)
            password = pushback;
        else
            hometown = pushback;
        i++;
    }
}
User::User(std::string user_name, std::string pass_word) : username(user_name), password(pass_word), hometown("")
{
    friends = new Bag<User>();
    pendingFriends = new Bag<User>();
}
User::User(std::string user_name, std::string pass_word, std::string home_town) : username(user_name), password(pass_word), hometown(home_town)
{
    friends = new Bag<User>();
    pendingFriends = new Bag<User>();
}
User::~User()
{
}
void User::addComment(int pos, std::string username, std::string comment)
{
    wall.getPost(pos)->addComment(username, comment);
}
void User::deleteComment(int wallpostPos, int commentPos, std::string username)
{
    wall.getPost(wallpostPos)->deleteComment(commentPos, username);
}
void User::deleteComment(int wallpostPos, int commentPos)
{
    wall.getPost(wallpostPos)->deleteComment(commentPos);
}
void User::setHometown(std::string aleph)
{
    hometown = aleph;
}
std::string User::getHometown() const
{
    return hometown;
}
std::string User::getUsername() const
{
    return username;
}
void User::setPassword(std::string pass)
{
    password = pass;
}
std::string User::getPassword() const
{
    return password;
}
void User::stringToWall(std::string strung)
{
    wall.StringToWall(strung);
}
void User::writePost(std::string postBody)
{
    time_t rawtime;
    WallPost alpha(username, postBody);
    alpha.setTimeCreated(time(&rawtime));
    wall.addPost(alpha);
}
void User::writePost(std::string author, std::string postBody)
{
    time_t rawtime;
    WallPost omega(author, postBody);
    omega.setTimeCreated(time(&rawtime));
    wall.addPost(omega);
}
bool sortProper(WallPost& a, WallPost &b)
{
    return (a.getTimeCreated() <= b.getTimeCreated());
}
bool sortByMostRecent(WallPost& a, WallPost &b)
{
    return (a.mostRecentTime() <= b.mostRecentTime());
}
void User::quickSort(int chosa)
{
    bool (*comparison)(WallPost&, WallPost&);
    if(chosa == 1)
    {
        comparison = sortProper;
    }
    else if(chosa == 2)
    {
        comparison = sortByMostRecent;
    }
    wall.quickSort(comparison, 0, wall.length()-1);
}
void User::deletePost(int number)
{
    try {
    wall.removePost(number);
    }
    catch(std::exception &e)
    {
        std::cout << e.what() << std::endl;
    }
}
void User::deletePost(std::string username, int number)
{
    try {
    wall.removePost(username, number);
    }
    catch(std::exception &e) { std::cout << e.what() << std::endl; }
}
std::string User::userDataToString() const
{
    return "%" + username + "." + password + "." + hometown + "\n";
}
void User::outputUserData() const
{
    std::cout << "User info:\nUsername: " << username << "\nPassword: " << password <<  "\nHometown: "<< hometown << std::endl;
}
void User::printWall()
{
    wall.WallToString(true);
}
std::string User::writeWallToFile()
{
    return wall.WallToString(false);
}
bool User::operator==(User const & other)
{
    return this->username == other.username;
}
void User::addFriend(const User& user)
{
    friends->push_back(user);
}
void User::removeFriend(User &user)
{
            friends->remove(user);
            user.friends->remove(*this);
}
void User::addPending(const User &user)
{
    pendingFriends->push_back(user);
}
void User::sendFriendRequest(User &user)
{
    if(!user.pendingFriends->contains(*this) && !user.friends->contains(*this))
    {
    user.pendingFriends->push_back(*this);
    }
}
void User::writePostOnFriendWall(std::string postbody, User &frienduser)
{
    if(frienduser.friends->contains(*this) && this->friends->contains(frienduser))
    {
        WallPost beta(username, postbody);
        frienduser.wall.addPost(beta);
    }
}
void User::respondToFriendRequest(User &dum, bool response)
{
    if(response)
    {
        if(!friends->contains(dum))
        {
            friends->push_back(dum);
            dum.friends->push_back(*this);
        }
    }
    pendingFriends->remove(dum);
    if(dum.pendingFriends->contains(*this))
    {
        dum.pendingFriends->remove(*this);
    }
}
std::string User::printPending(bool writeOrPrint)
{
    std::string varchar ="";
    for(BagIterator<User> mu = pendingFriends->begin(); mu!=pendingFriends->end(); mu++)
    {
        if(writeOrPrint)
        std::cout << (*mu).getUsername() << std::endl;
        else
        {
            varchar= varchar + getUsername() + ">" + (*mu).getUsername();
            varchar+="\n";
        }
    }
    return varchar;
}
std::string User::printFriends(bool writeOrPrint)
{
    std::string varchar = "";
    for(BagIterator<User> mu = friends->begin(); mu!=friends->end(); mu++)
    {
        if(writeOrPrint)
        {
        std::cout << (*mu).getUsername() << std::endl;
        }
        else
        {
            varchar= varchar + getUsername() + ":" + (*mu).getUsername();
            varchar+="\n";
        }
    }
    return varchar;
}
