#include <iostream>
#include "WallPost.h"
#include <sstream>
int generateTimeStamp()
{
    time_t rawtime;
    int times = time(&rawtime);
    return times;
}
WallPost::WallPost(std::string authornm, std::string bodymsg, int tempus) : body(bodymsg), authorname(authornm), datetime(tempus)
{
    
}
WallPost::WallPost(std::string authorName, std::string bodyMsg) : body(bodyMsg), authorname(authorName)
{

}
WallPost::WallPost() : body(""), authorname("")
{

}
WallPost::~WallPost()
{
}
void WallPost::setBody(std::string message)
{
	body = message;
}
std::string WallPost::getBody() const
{
	return body;
}
void WallPost::setAuthor(std::string authorNm)
{
	authorname = authorNm;
}
std::string WallPost::getAuthor() const
{
	return authorname;
}
int WallPost::getPostNumber() const
{
	return postNumber;
}
void WallPost::setPostNumber(int post)
{
	postNumber = post;
}
int WallPost::getTimeCreated() const
{
	return datetime;
}
void WallPost::setTimeCreated(int time)
{
	datetime = time;
}
void WallPost::printWallPost() const
{
    time_t posttime = (time_t)getTimeCreated();
    std::cout << "Post #" << postNumber << " made at " << ctime(&posttime) << "by " << authorname << ": " << body << std::endl;
    if(comments.size() > 0)
    {
    for(int i = 0; i < comments.size(); i++)
    {
        time_t comment_time = (time_t)comments[i].getTimeCreated();
        std::cout << "Comment #" << i+1 << " made at " << ctime(&comment_time) << "by " << comments[i].getPoster() << ": " << comments[i].getCommentBody() << std::endl;
    }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}
std::string WallPost::formatWallPost() const
{
    std::string post = "";
    std::stringstream ss;
    ss << datetime;
    post = "*" + authorname + "|" + body + "|" + ss.str() + "|";
    for(int i = 0; i < comments.size(); i++)
    {
        std::stringstream st;
        st << comments[i].getTimeCreated();
        post = post + comments[i].getCommentBody() + "`" + comments[i].getPoster() + "`" + st.str() + "`>";
    }
    return post;
}
void WallPost::addComment(std::string username, std::string commentBody) //when created by user
{
    wallpostComment comment(username, commentBody);
    comment.setTimeCreated(generateTimeStamp());
    comments.push_back(comment);
}
void WallPost::addComment(wallpostComment& comment) //when read from file
{
    comments.push_back(comment);
}
void WallPost::deleteComment(int pos, std::string username)
{
    if(pos < 0 || pos > comments.size()) { throw std::logic_error("Out of bounds error."); }
    if(comments[pos].getPoster()==username)
    {
        comments.erase(comments.begin()+pos);
    }
    else
    {
        std::cout << "Unauthorized to delete this comment!" << std::endl;
    }
}
int WallPost::mostRecentTime() const
{
    int createTime = datetime;
    for(int i = 0; i < comments.size(); i++)
    {
        if(comments[i].getTimeCreated() > createTime)
        {
            createTime = comments[i].getTimeCreated();
        }
    }
    return createTime;
}
void WallPost::deleteComment(int pos)
{
    if(pos < 0 || pos > comments.size()) { throw std::logic_error("Out of bounds error."); }
    comments.erase(comments.begin()+pos);
}