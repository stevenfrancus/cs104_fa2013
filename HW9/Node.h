//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

template <class KeyType, class ValueType>
class Node {
public:
    int numberOfKeys;
    KeyType leftKey, rightKey;
    ValueType leftValue, rightValue;
    int leftChild, middleChild, rightChild, parent;
    Node (const KeyType & key, const ValueType & value); // one key only
    Node (const KeyType & lKey, const ValueType & lValue,
          const KeyType & rKey, const ValueType & rValue); // two keys
    Node();
    ~Node ();
};
template <class KeyType, class ValueType>
Node<KeyType, ValueType>::Node(const KeyType & key, const ValueType & value) : leftKey(key), leftValue(value), leftChild(-4), middleChild(-4), rightChild(-4), numberOfKeys(1), parent(-1)
{
}
template <class KeyType, class ValueType>
Node<KeyType, ValueType>::Node (const KeyType & lKey, const ValueType & lValue,
            const KeyType & rKey, const ValueType & rValue) : leftKey(lKey), leftValue(lValue), rightKey(rKey), rightValue(rValue), leftChild(-4), middleChild(-4), rightChild(-4), numberOfKeys(2), parent(-1)
{
}
template <class KeyType, class ValueType>
Node<KeyType, ValueType>::~Node()
{
}
template <class KeyType, class ValueType>
Node<KeyType, ValueType>::Node() : leftChild(-4), middleChild(-4), rightChild(-4), numberOfKeys(1), parent(-1)
{
}