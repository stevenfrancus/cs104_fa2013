//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <sstream>
template <typename T>
void quickSort(T *arr, bool(*compare)(T&, T&), int left_bound, int right_bound);
