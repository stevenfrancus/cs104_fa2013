//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include "quickSort.h"
#include "quickSort.cpp"
int total = 0;
bool checkSum(int& a, int& b) //compares the two numbers, but instead of sorting them just checks if they equal the total and then prints them out.
{
    
    if(a + b == total)
    {
        std::cout << total << " = " << a << " + " << b << std::endl;
        a = b = NULL;
    }
}
bool compareSort(int &a, int &b)
{
    return (a < b);
}
int main(int argc, const char * argv[])
{

    std::ifstream aus;
    aus.open("numbers.txt", std::ios::in);
    int counter = 0;
    int length = 0;
    int *numArray;
    while(aus.good())
    {
        std::string len = "";
        std::getline(aus, len);
        if(counter==0)
        {
            int count = 0;
            int lum = 0;
            std::stringstream str(len);
            while (str >> lum)
            {
                if(count==0) { numArray = new int[lum]; length = lum; }
                else {
                    total = lum;
                }
                count++;
            }
        }
        else {
            int number = 0;
            std::stringstream str(len);
            str >> number;
            numArray[counter-1] = number;
        }
        counter++;
    }
    bool (*compare)(int&, int&);
    compare = checkSum;
    quickSort<int>(numArray, compare, 0, length-1);
    delete []numArray;
    numArray = NULL;
    return 0;
}

