//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include "Node.h"
#include <vector>
#include <iostream>
#include <stdio.h>
#include <stdexcept>
template <typename KeyType, typename ValueType>
class TwoThreeTree : private std::vector< Node<KeyType, ValueType> > {
public:
    TwoThreeTree()
    {
        
    };
    ~TwoThreeTree()
    {
        
    };
    void add (const KeyType & key, const ValueType & value);
    void remove (const KeyType & key);
    ValueType & get (const KeyType & key);
private:
void fix(int pos);
void split(int pos, const KeyType & key, const ValueType & value);
int locateLeaf(const KeyType & key, int pos) const;
bool isLeaf(int pos) const;
};
template <class KeyType, class ValueType>
void TwoThreeTree<KeyType, ValueType>::add (const KeyType & key, const ValueType & value)
{
    int size = this->size();
    if(size==0)
    {
        Node<KeyType,ValueType> nodulus(key, value);
        (*this).push_back(nodulus);
        return;
    }
    int index = locateLeaf(key);
    if((*this)[index].numberOfKeys==1)
    {
        if(key < (*this)[index].leftKey)
        {
            KeyType leftK = (*this)[index].leftKey;
            ValueType leftV = (*this)[index].leftValue;
            (*this)[index].leftKey = key;
            (*this)[index].leftValue = value;
            (*this)[index].rightKey = leftK;
            (*this)[index].rightValue = leftV;
        }
        else if(key > (*this)[index].leftKey)
        {
            (*this)[index].rightKey = key;
            (*this)[index].rightValue = value;
        }
        (*this)[index].numberOfKeys++;
    }
    else if((*this)[index].numberOfKeys == 2)
    {
        KeyType keySent = (*this)[index].leftKey;
        ValueType valu = (*this)[index].leftValue;
        KeyType keyS = (*this)[index].rightKey;
        ValueType valS = (*this)[index].rightValue;
        if(key < (*this)[index].leftKey)
        {
            (*this)[index].leftKey = key;
            (*this)[index].leftValue = value;
            split(index, keySent, valu);
        }
        else if(key > (*this)[index].rightKey)
        {
            (*this)[index].rightKey = key;
            (*this)[index].rightValue = value;
            split(index, keyS, valS);
        }
        else { split(index, key, value); }
    }
    
}
template <class KeyType, class ValueType>
int TwoThreeTree<KeyType, ValueType>::locateLeaf(const KeyType & key, int pos = 0) const
{
    if(this->size()==0) { throw std::logic_error("Empty tree."); }
    
    if(isLeaf(pos) || (*this)[pos].leftKey==key || (*this)[pos].rightKey==key)
    {
        return pos;
    }
    else
    {
       if((*this)[pos].numberOfKeys == 1)
       {
           if((*this)[pos].leftKey > key)
           {
               return locateLeaf(key, (*this)[pos].leftChild);
           }
           else
               return locateLeaf(key, (*this)[pos].rightChild);
       }
       else if((*this)[pos].numberOfKeys == 2)
        {
        if((*this)[pos].leftKey > key)
        {
            return locateLeaf(key, (*this)[pos].leftChild);
        }
        else if((*this)[pos].rightKey > key)
        {
            return locateLeaf(key, (*this)[pos].middleChild);
        }
        else
        {
            return locateLeaf(key, (*this)[pos].rightChild);
        }
    }
    }
}
template <class KeyType, class ValueType>
void TwoThreeTree<KeyType, ValueType>::split(int pos, const KeyType & key, const ValueType & value)
{
    KeyType empty;
    ValueType emptyVal;
    if((*this)[pos].parent < 0) //root
    {
        if(key < (*this)[pos].leftKey)
        {
            Node<KeyType,ValueType> pLeft(key,value);
            pLeft.parent = pos;
            Node<KeyType,ValueType> pRight((*this)[pos].rightKey, (*this)[pos].rightValue);
            pRight.parent = pos;
            (*this)[pos].rightKey = empty;
            (*this)[pos].rightValue = emptyVal;
            (*this)[pos].numberOfKeys--;
            this->push_back(pLeft);
            (*this)[pos].leftChild = this->size()-1;
            this->push_back(pRight);
            (*this)[pos].rightChild = this->size()-1;
        }
        else if(key < (*this)[pos].rightKey)
        {
            Node<KeyType,ValueType> pLeft((*this)[pos].leftKey,(*this)[pos].leftValue);
            pLeft.parent = pos;
            Node<KeyType,ValueType> pRight((*this)[pos].rightKey, (*this)[pos].rightValue);
            pRight.parent = pos;
            Node<KeyType,ValueType> newPos(key, value);
            (*this)[pos] = newPos;
            this->push_back(pLeft);
            (*this)[pos].leftChild = this->size()-1;
            this->push_back(pRight);
            (*this)[pos].rightChild = this->size()-1;
        }
        else
        {            
            Node<KeyType,ValueType> pLeft((*this)[pos].leftKey,(*this)[pos].leftValue);
            pLeft.parent = pos;
            Node<KeyType,ValueType> pRight(key, value);
            pRight.parent = pos;
            
            Node<KeyType,ValueType> newPos((*this)[pos].rightKey, (*this)[pos].rightValue);
            (*this)[pos] = newPos;
            this->push_back(pLeft);
            (*this)[pos].leftChild = this->size()-1;
            this->push_back(pRight);
            (*this)[pos].rightChild = this->size()-1;
        }
    }
    /*if((*this)[(*this)[pos].parent].numberOfKeys == 1)
    {
        if(key > (*this)[(*this)[pos].parent].leftKey)
        {
            (*this)[(*this)[pos].parent].rightKey = key;
            (*this)[(*this)[pos].parent].rightValue = value;
        }
        else
        {
            (*this)[(*this)[pos].parent].rightKey = (*this)[(*this)[pos].parent].leftKey;
            (*this)[(*this)[pos].parent].rightValue = (*this)[(*this)[pos].parent].leftValue;
            (*this)[(*this)[pos].parent].leftKey = key;
            (*this)[(*this)[pos].parent].leftValue = value;
        }
    }*/
    else
    {
        if(key < (*this)[pos].leftKey)
        {
            KeyType cle = (*this)[pos].leftKey;
            ValueType cal = (*this)[pos].leftValue;
            (*this)[pos].leftKey = key;
            (*this)[pos].leftValue = value;
            split((*this)[pos].parent, cle, cal);
        }
        else if(key < (*this)[pos].leftKey)
        {
            split((*this)[pos].parent, key, value);
        }
        else
        {
            KeyType cle = (*this)[pos].rightKey;
            ValueType cal = (*this)[pos].rightValue;
            (*this)[pos].rightKey = key;
            (*this)[pos].rightValue = value;
            split((*this)[pos].parent, cle, cal);
        }
        
    }

}
template <class KeyType, class ValueType>
void TwoThreeTree<KeyType, ValueType>::fix(int pos)
{
    if((*this)[pos].numberOfKeys < 1 && (*this)[pos].parent != -1)
    {
        int parentIndex = (*this)[pos].parent;
        int _right = (*this)[parentIndex].rightChild;
        int _left = (*this)[parentIndex].leftChild;
        int _middle = (*this)[parentIndex].middleChild;
        if(_right!= -4 && _right!=pos ){}
        
    }
}

template <class KeyType, class ValueType>
void TwoThreeTree<KeyType, ValueType>::remove (const KeyType & key)
{
    KeyType keyEmpty;
    ValueType valueEmpty;
    if(this->size()==0) { throw std::logic_error("Tree empty."); }
    int index = locateLeaf(key);
    if((*this)[index].leftKey == key && (*this)[index].rightKey != keyEmpty)
    {
        (*this)[index].leftKey = (*this)[index].rightKey;
        (*this)[index].leftValue = (*this)[index].rightValue;
        (*this)[index].rightKey = keyEmpty;
        (*this)[index].rightValue = valueEmpty;
    }
    else if((*this)[index].rightKey == key)
    {
        (*this)[index].rightKey = keyEmpty;
        (*this)[index].rightValue = valueEmpty;
    }
    (*this)[index].numberOfKeys--;
    if((*this)[index].numberOfKeys < 1)
    {
       //fix(index);
    }
}
template <class KeyType, class ValueType>
bool TwoThreeTree<KeyType,ValueType>::isLeaf(int pos) const
{
    if((*this)[pos].leftChild==-4 && (*this)[pos].rightChild==-4 && (*this)[pos].middleChild==-4) return true;
    else
    return false;
}
template <class KeyType, class ValueType>
ValueType & TwoThreeTree<KeyType, ValueType>::get (const KeyType & key)
{
    if(this->size() == 0) throw std::logic_error("Empty tree.");
    int index = locateLeaf(key);
    std::cout << index << std::endl;
    if(index > this->size()) { throw std::logic_error("Invalid position."); }
    if((*this)[index].leftKey==key)
    {
        return (*this)[index].leftValue;
    }
    else if((*this)[index].rightKey == key)
    {
        return (*this)[index].rightValue;
    }
    else
        throw std::logic_error("Item not found");
}
