//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include "twoThreeTree.h"
class studentInfo
{
public:
    studentInfo(std::string n) : studentName(n) {};
    studentInfo() : studentName("") {};
    std::string studentName;
    bool operator<(const studentInfo & other) const
    {
        int traverseStrings = 0;
        std::string thisName = this->studentName;
        std::string otherName = other.studentName;
        std::transform(thisName.begin(), thisName.end(), thisName.begin(), ::tolower);
        std::transform(otherName.begin(), otherName.end(), otherName.begin(), ::tolower);
        int minLength = (int)std::min(thisName.length(), otherName.length());
        while (traverseStrings < minLength)
        {
            if (thisName[traverseStrings] < otherName[traverseStrings]) return true;
            else if (thisName[traverseStrings] > otherName[traverseStrings]) return false;
            else
                ++traverseStrings;
        }
        return false; //this probably means the strings are equal
    };
    bool operator==(const studentInfo & other) const
    {
        return this->studentName == other.studentName;
    };
    bool operator>(const studentInfo & other) const
    {
        return (this->studentName!=other.studentName && !(*this<other));
    };
    bool operator!=(const studentInfo &other) const
    {
        return !(*this==other);
    };
    studentInfo& operator=(const studentInfo& p)
    {
        this->studentName = p.studentName;
    };
};
int main(int argc, const char * argv[])
{
    TwoThreeTree<studentInfo, double> studentGPATree;
    while(true)
    {
        std::cout << "Would you like to:\n1. add a student record\n2. remove a student record \n3. Look up a student\n4. exit?\n";
        int choice = 0;
        std::cin >> choice;
        std::cin.ignore();
        if(choice == 1)
        {
            double gpa = 0.0;
            std::string studentNam, studentI;
            std::cout << "What is the student's name?" << std::endl;
            std::getline(std::cin, studentNam);
            std::cout << "What is the student's GPA?" << std::endl;
            std::getline(std::cin, studentI);
            std::stringstream str(studentI);
            str >> gpa;
            studentInfo newStudent(studentNam);
            try{
                studentGPATree.add(newStudent, gpa);
            }
            catch(std::exception &e) { std::cout << e.what() << std::endl; }
            
        }
        else if(choice == 2)
        {
            std::string studentN = "";
            std::cout << "What is the student's name?" << std::endl;
            std::getline(std::cin, studentN);
            studentInfo deleteStudent(studentN);
            try {
                studentGPATree.remove(deleteStudent);
            }
            catch(std::exception &e)
            {
                std::cout << e.what() << std::endl;
            }
        }
        else if(choice == 3)
        {
            bool failed = false;
            std::string studentN = "";
            std::cout << "What is the student's name?" << std::endl;
            std::getline(std::cin, studentN);
            studentInfo Student(studentN);
            double gpa = 0.0;
            try {
                gpa = studentGPATree.get(Student);
            }
            catch(std::exception &e)
            {
                failed = true;
                std::cout << e.what() << std::endl;
            }
            if(!failed)
            std::cout << studentN << "'s GPA is: " << gpa << std::endl;
        }
        else
        {
            break;
        }
    }
    return 0;
}

