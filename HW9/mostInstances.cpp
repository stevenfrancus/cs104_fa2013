//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include "quickSort.h"
#include "quickSort.cpp"
class keyValue
{
public:
    std::string word;
    int number;
};
void compareCheck(std::string *arr, int length)
{
    keyValue* table = new keyValue[length];
    int j = 1;
    for(int i = 0; i < length; i++)
    {
        if(arr[i]!=table[j-1].word)
        {
            table[j].word = arr[i];
            table[j].number = 1;
            j++;
        }
        else
        {
            table[j-1].number++;
        }
    }
    int max = 0;
    for(int i = 0; i < length; i++)
    {
        if(table[i].word!="")
        {
        if(table[i].number > max)
        {
            max = table[i].number;
        }
        }
    }
    for(int i = 0; i < length; i++)
    {
        if(table[i].word!="" && table[i].number==max)
        {
           std::cout << table[i].word << std::endl;
        }
    
    }
    delete []table;
    table = NULL;
}
bool alphabetOrder(std::string &a, std::string &b) //returns true if a occurs before b, false otherwise
{
    int minLength = std::min((int)a.length(), (int)b.length());
    int counter = 0;
    while(counter < minLength)
    {
        if(a[counter] < b[counter])
        {
            return true;
        }
        else if(a[counter] > b[counter])
        {
            return false;
        }
        counter++;
    }
    if (a.length() < b.length())
    {
        return true;
    }
    else {
    return false;
    }
}


int main(int argc, const char * argv[])
{

    std::ifstream aus;
    aus.open("testfile.txt", std::ios::in);
    int counter = 0;
    int length = 0;
    std::string *stringArray;
    while(aus.good())
    {
        std::string len = "";
        std::getline(aus, len);
        if(counter==0)
        {
            std::stringstream str(len);
            str >> length;
            stringArray = new std::string[length];
        }
        else {
            stringArray[counter-1] = len;
        }
        counter++;
    }
    bool (*compare)(std::string&, std::string&);
    compare = alphabetOrder;
    quickSort<std::string>(stringArray, compare, 0, length-1);
    compareCheck(stringArray, length);
    
    delete []stringArray;
    stringArray = NULL;
    return 0;
}

