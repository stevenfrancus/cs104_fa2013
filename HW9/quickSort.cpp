//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include "quickSort.h"
#include <cstdlib>
#include <ctime>

template <typename T>
int partition(T *arr, bool(*compare)(T&, T&), int left_bound, int right_bound)
{
    int i = left_bound;
    for(int j = left_bound; j < right_bound; j++)
    {
        if(compare(arr[j], arr[right_bound]))
        {
            T arrI = arr[i];
            T arrJ = arr[j];
            arr[i] = arr[j];
            arr[j] = arrI;
            i++;
        }
    }
    T arrI = arr[i];
    T arrJ = arr[right_bound];
    arr[i] = arr[right_bound];
    arr[right_bound] = arrI;
    return i;
}
template <typename T>
void quickSort(T *arr, bool(*compare)(T&, T&), int left_bound, int right_bound)
{
    srand(time(NULL));
    if(left_bound < right_bound)
    {
        int p = partition(arr, compare, left_bound, right_bound);
        quickSort(arr, compare, left_bound, p-1);
        quickSort(arr, compare, p+1, right_bound);
    }
}