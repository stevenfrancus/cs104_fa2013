//
//  main.cpp
//  LinkedListTemplate
//
//  Created by Steven Francus on 9/19/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include <iostream>
#include "node.h"
#include "Wall.h"
#include "User.h"
#include "UserList.h"
#include "PageList.h"
#include <cstdlib>
#include <ctime>
#include <fstream> 
#include "md5.h"
#include <algorithm>
using namespace std;

int firstIndexOfChars(std::string str, char character)
{
    for(int i = 0; i < str.length(); i++)
    {
        if(str[i]==character)
        {
            return i;
        }
    }
    return -1;
}
void socialnetwork(UserList &userlist, PageList &pages, User &user)
{
    int choice = 0;
    std::cout << "Would you like to:\n1. See and/or modify all your existing wall posts.\n2. Create a new wall post.\n3. Delete an existing wall post by number.\n4. Change your password or hometown.\n5. Search for an existing user via real name or via username to send a friend request to.\n6. See the connections you have with another user.\n7. Look at all pending friend requests.\n8. See all friends.\n9. Create a new page.\n10. Delete an existing page.\n11. See all existing pages.\n12. See all currently liked pages\n13. Log out.\n";
    std::cin >> choice;
    std::cin.ignore();
    if(std::cin)
    {
        switch(choice)
        {
            case 1:
            {
                while(true)
                {
                    user.printWall();
                    std::cout << "Would you like to:\n1. Add a comment\n2. Delete a comment (note: since this is your wall you can delete any comments, not just ones you have made)\n3. Exit." << std::endl;
                    int cosum = 0;
                    std::cin >> cosum;
                    std::cin.ignore();
                    if(cosum==3) { break; }
                    if(cosum==1)
                    {
                        std::cout << std::endl;
                        //:/ <--this code is not happy
                        std::cout << "Please enter the number of a wall post if you would like to comment on it." << std::endl;
                        int chosa = 0;
                        std::string commentBody = "";
                        std::cin >> chosa;
                        std::cin.ignore();
                        std::cout << "Please enter the body of the comment you would like to write." << std::endl;
                        std::getline(std::cin, commentBody);
                        try {
                            user.addComment(chosa-1, user.getName(), commentBody);
                        }
                        catch(std::exception &e) {
                            std::cout << e.what() << std::endl;
                        }
                    }
                    else if(cosum==2)
                    {
                        int postNum = 0;
                        int commentNum = 0;
                        std::cout << "Please type in the number of the post you would like to delete a comment from." << std::endl;
                        std::cin >> postNum;
                        std::cin.ignore();
                        std::cout << "Please type in the number of the comment you would like to remove." << std::endl;
                        std::cin >> commentNum;
                        std::cin.ignore();
                        try {
                            user.deleteComment(postNum-1, commentNum-1);
                        }
                        catch(std::exception &e)
                        {
                            std::cout << e.what() << std::endl;
                        }
                    }
                }
                socialnetwork(userlist, pages, user);
                break;
            }
            case 2:
            {
                std::string postbody = "";
                std::cout << "Please enter the body of the wall post you want to write.\n";
                getline(std::cin, postbody);
                user.writePost(postbody);
                socialnetwork(userlist, pages, user);
                break;
            }
            case 3:
            {
                int numOfPost = -1;
                std::cout << "Please enter the number of the wall post you would like to delete.\n";
                std::cin >> numOfPost;
                try {
                    user.deletePost(numOfPost);
                }
                catch(std::exception &e)
                {
                    std::cout << e.what() << std::endl;
                }
                socialnetwork(userlist, pages, user);
                break;
            }
            case 4:
            {
                int cosa = 0;
                std::string newHometown = "";
                std::string newPassword = "";
                std::cout << "Would you like to change your: 1. password 2. hometown?\n";
                std::cin >> cosa;
                if(cosa == 1)
                {
                    std::cout << "Please enter a new password:\n";
                    std::cin >> newPassword;
                    auto hash = md5(newPassword.c_str(), newPassword.size()*sizeof(char));
                    user.setPassword(hash.toString());
                    socialnetwork(userlist, pages, user);
                }
                else if(cosa == 2)
                {
                    
                    std::cout << "Please enter a new hometown:\n";
                    std::cin >> newHometown;
                    user.setHometown(newHometown);
                    socialnetwork(userlist, pages, user);
                }
                else
                {
                    std::cout << "Invalid input detected. Please choose again.\n";
                    socialnetwork(userlist, pages, user);
                }
                break;
            }
            case 5:
            {
                int choice = 0;
                std::cout << "Would you like to search for a user by:\n1. Full username\n2. Full or part of their name" << std::endl;
                std::cin >> choice;
                std::cin.clear();
                std::cin.sync();
                std::cin.ignore();
                if(choice == 1)
                {
                    std::string username_add;
                    std::cout << "Please enter their username to add a user as a friend." << std::endl;
                    std::getline(std::cin, username_add);
                    User *friendu;
                    try {
                        friendu = userlist.findUser(username_add);
                        if(friendu==nullptr)
                        {
                            std::cout << "The username you have typed does not exist. Please try again." << std::endl;
                            socialnetwork(userlist, pages, user);
                        }
                    }
                    catch(std::exception &e)
                    {
                        
                    }
                    user.sendFriendRequest(*friendu);
                }
                else{
                    std::string username_substring = "";
                    std::cout << "Please enter at least part of a name to search for users." << std::endl;
                    std::cin >> username_substring;
                    std::transform(username_substring.begin(), username_substring.end(), username_substring.begin(), ::tolower);
                    std::cout << "Please select which user to send a friend request to. Please type in their name to send a friend request to them:" << std::endl;
                    std::string u_n;
                    for(auto it = userlist.begin(); it!= userlist.end(); it++)
                    {
                        std::transform(username_substring.begin(), username_substring.end(), username_substring.begin(), ::tolower);
                        std::string u_name = (*it).value.getName();
                        std::transform(u_name.begin(), u_name.end(), u_name.begin(), ::tolower);
                        if(u_name.find(username_substring)!=std::string::npos)
                        {
                            std::cout << (*it).value.getUsername() << " (" << (*it).value.getName() << ") " << std::endl;
                        }
                    }
                    std::cout << "Please type in their username to add them as a friend (username is the part not in parenthesis)." << std::endl;
                    std::cin.ignore();
                    std::cin.sync();
                    std::cin.clear();
                    std::getline(std::cin, u_n);
                    if(u_n == "") { break; }
                    User *frand;
                    try {
                        frand = userlist.findUser(u_n);
                        if(frand==NULL) { socialnetwork(userlist, pages, user); }
                    }
                    catch(std::exception &e) {  }
                    user.sendFriendRequest(*frand);
                }
                socialnetwork(userlist, pages, user);
                break;
            }
            case 6:
            {
                std::cout << "Here is a list of all users currently in the database." << std::endl;
                for(auto it = userlist.begin(); it!= userlist.end(); it++)
                {
                    std::cout << (*it).value.getUsername() << std::endl;
                }
                std::string choseString = "";
                std::cout << "Please enter a username to see all the connections you have to him/her or 'q' to quit." << std::endl;
                std::getline(std::cin, choseString);
                if(choseString=="q"||choseString=="Q")
                {
                    socialnetwork(userlist, pages, user);
                }
                else
                {
                    if(userlist.containsUser(choseString))
                    {
                        userlist.shortestPath(user.getUsername(), choseString);
                    }
                }
                break;
            }
            case 7:
            {
                std::string friendString = "";
                bool yesOrNo = false;
                std::cout << "Here are all pending friend requests: "<< std::endl;
                user.printPending(true);
                std::cout << "Please type in the username of the person followed by a ',' followed by either 'yes' to add them to your friends list or 'del' to delete the pending request (i.e. Steven Francus,yes) or any key to quit." << std::endl;
                std::cin.clear();
                std::getline(std::cin, friendString);
                int nexi = firstIndexOfChars(friendString, ',');
                std::string username = friendString.substr(0, nexi);
                std::string answer = friendString.substr(nexi+1, friendString.length());
                std::transform(answer.begin(), answer.end(), answer.begin(), ::tolower);
                if(answer=="yes") { yesOrNo = true; }
                else if(answer=="del") { yesOrNo = false; }
                else { break; }
                User *userd = userlist.findUser(username);
                if(userd == NULL) { break; }
                user.respondToFriendRequest(*userd, yesOrNo);
                socialnetwork(userlist, pages, user);
                break;
            }
            case 8:
            {
                int checkQuickSort = 0;
                std::string delString = "";
                std::cout << "Here are all your friends: " << std::endl;
                user.printFriends(true);
                std::cout << "To choose a friend, please enter their username below. To exit, please type in 'q'." << std::endl;
                std::getline(std::cin, delString);
                if(delString=="q") { break; }
                User *userd = userlist.findUser(delString);
                try {
                    int choose_again = 0;
                    std::cout << "Would you like to:\n1. look at this user's wall and comment on posts\n2. See what this user likes.\n3. delete this user from your friends list." << std::endl;
                    std::cin >> choose_again;
                    std::cin.ignore();
                    switch(choose_again)
                    {
                        case 1:
                        {
                            std::cout << "How would you like to sort " << userd->getUsername() << "'s wall:\n1) By most recent wall post last\n2) By wall post with most recent comment last?" << std::endl;
                            std::cin >> checkQuickSort;
                            std::cin.ignore();
                            if(checkQuickSort == 1 || checkQuickSort == 2)
                            {
                                userd->quickSort(checkQuickSort);
                            }
                            else
                            {
                                std::cout << "Invalid answer. Sorting default..." << std::endl;
                            }
                            std::cout << "Here is " + userd->getName() + "'s wall: " << std::endl;
                            userd->printWall();
                            std::cout << std::endl;
                            std::cout << "Would you like to:\n1. Delete a wall post or the comments on it that you have created\n2. Write a wall post on their wall\n3. Select a wall post to comment on (or delete your own comments)" << std::endl;
                            int choose_again_again = 0;
                            std::cin >> choose_again_again;
                            std::cin.ignore();
                            switch(choose_again_again)
                            {
                                case 1:
                                {
                                    int blade = 0;
                                    std::cout << "Would you like to:\n1. Delete a post you have made\n2. Delete a comment you have made\n3. Exit." << std::endl;
                                    std::cin >> blade;
                                    std::cin.ignore();
                                    switch(blade){
                                        case 1:
                                        {
                                            std::cout << "Please enter the number of the wall post you would like to delete." << std::endl;
                                            std::cin >> choose_again_again;
                                            std::cin.ignore();
                                            try
                                            {
                                                userd->deletePost(user.getName(), choose_again_again);
                                            }
                                            catch(std::exception &e)
                                            {
                                                std::cout << e.what() << std::endl;
                                            }
                                            break;
                                        }
                                        case 2:
                                        {
                                            int wallpostNum = 0;
                                            int commentNum = 0;
                                            std::cout << "Please enter the number of the wall post which contains the comment you would like to delete." << std::endl;
                                            std::cin >> wallpostNum;
                                            std::cin.ignore();
                                            std::cout << "Please enter the number of the comment you would like to delete." << std::endl;
                                            std::cin >> commentNum;
                                            std::cin.ignore();
                                            try
                                            {
                                                userd->deleteComment(wallpostNum-1, commentNum-1, user.getName());
                                            }
                                            catch(std::exception &e)
                                            {
                                                std::cout << e.what() << std::endl;
                                            }
                                            
                                            break;
                                        }
                                        default:
                                        {
                                            break;
                                        }
                                    }
                                    break;
                                }
                                case 2:
                                {
                                    std::cout << "Please enter the body of the post to write it to " << userd->getUsername() << "'s wall.\t" << std::endl;
                                    std::string wallBody = "";
                                    std::getline(std::cin, wallBody);
                                    userd->writePost(user.getName(), wallBody);
                                    break;
                                }
                                case 3:
                                {
                                    while(true)
                                    {
                                        std::cout << std::endl;
                                        //:/ <--this code is not happy
                                        std::cout << "Please enter the number of a wall post if you would like to comment on it. Otherwise type '-1' to exit." << std::endl;
                                        int chosa = 0;
                                        std::string commentBody = "";
                                        std::cin >> chosa;
                                        std::cin.ignore();
                                        if(chosa == -1) { break; }
                                        std::cout << "Please enter the body of the comment you would like to write." << std::endl;
                                        std::getline(std::cin, commentBody);
                                        try {
                                            userd->addComment(chosa-1, user.getName(), commentBody);
                                        }
                                        catch(std::exception &e) {
                                            std::cout << e.what() << std::endl;
                                        }
                                    }
                                    break;
                                }
                                default:
                                {
                                    break;
                                }
                                    
                            }
                            break;
                        }
                        case 2:
                        {
                            userd->printPagesLiked();
                            break;
                        }
                        case 3:
                        {
                            user.removeFriend(*userd);
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                }
                catch(std::exception &e)
                {
                    std::cout << e.what() << std::endl;
                }
                if(checkQuickSort!=1)
                {
                    userd->quickSort(1); //puts it back in the normal order
                }
                socialnetwork(userlist, pages, user);
                break;
            }
            case 9:
            {
                std::string name, blurb, about;
                std::cout << "You have selected to create a new page. Please give the name of the page now." << std::endl;
                std::getline(std::cin, name);
                std::cin.clear();
                std::cout << "Please write a short statement about the page." << std::endl;
                std::getline(std::cin, blurb);
                std::cin.clear();
                std::cout << "Please write an about statement about the page." << std::endl;
                std::getline(std::cin, about);
                std::cin.clear();
                user.createPage(pages, name, blurb, about);
                break;
            }
            case 10:
            {
                std::string delPage, pageName;
                std::cout << "You have selected to delete a page. Are you sure you want to delete a page. Press 'q' to quit or 'y' to continue.";
                std::getline(std::cin, delPage);
                std::cin.clear();
                if(delPage == "y")
                {
                    std::cout << "Here are a list of pages you currently have ownership of:" << std::endl;
                    user.printPagesLiked();
                    std::cout << "Please type in the name of the page you would like to delete. You can only delete pages which are marked as owned. Note: do not inclue the (owned)." << std::endl;
                    std::cin.clear();
                    std::cin.sync();
                    std::cin.ignore();
                    std::getline(std::cin, pageName);
                    try
                    {
                        user.deletePage(pages, pageName);
                    }
                    catch(std::exception &e)
                    {
                        std::cout << e.what() << std::endl;
                    }
                }
                else
                {
                    socialnetwork(userlist, pages, user);
                }
                break;
            }
            case 11:
            {
                int choice;
                string substr;
                std::cout << "Would you like to:\n1. See all existing pages.\n2. Search for a page by inputting part of its name." << std::endl;
                std::cin >> choice;
                std::cin.clear();
                if(choice == 2)
                {
                    std::cout << "Please enter part of the name to search for it." << std::endl;
                    std::cin >> substr;
                    std::cin.clear();
                     std::transform(substr.begin(), substr.end(), substr.begin(), ::tolower);
                }
                for(BagIterator<Page> it = pages.begin(); it!=pages.end(); it++)
                {
                    if(choice == 1)
                    {
                        std::cout << (*it).getName() << std::endl;
                    }
                    else
                    {
                        std::string pageName = (*it).getName();
                    std::transform(pageName.begin(), pageName.end(), pageName.begin(), ::tolower);
                        if(pageName.find(substr)!=std::string::npos)
                        {
                            std::cout << (*it).getName() << std::endl;
                        }
                    }
                }
                std::cin.ignore();
                std::cin.sync();
                std::cin.clear();
                std::string likePage;
                std::cout << "Do you want to learn more about a page? Type the name of the page you want to learn more about below to view it." << std::endl;
                std::getline(std::cin, likePage);
                bool found = false;
                Page* pagina;
                for(BagIterator<Page> it = pages.begin(); it!=pages.end(); it++)
                {
                    if(likePage == (*it).getName())
                    {
                        pagina = &(*it);
                        found = true;
                        break;
                    }
                }
                if(found)
                {
                    pagina->printPage();
                    std::string toLike;
                    std::cout << "Type 'like' to like this page. Type anything else to exit." << std::endl;
                    std::getline(std::cin, toLike);
                    if(toLike == "like" || toLike == "Like" || toLike == "LIKE")
                    {
                    user.like(*pagina);
                    }
                }
                socialnetwork(userlist, pages, user);
                break;
            }
            case 12:
            {
                user.printPagesLiked();
                int choice;
                std::string entry;
                std::cout << "1. Would you like to stop liking a page?\n2. Exit." << std::endl;
                cin >> choice;
                std::cin.ignore();
                std::cin.clear();
                if(choice == 1)
                {
                    std::string delString;
                    std::cout << "Please type in the name of a page in order to stop liking it. Typing anything else won't do anything. Note: do not include the (owned)." << std::endl;
                    std::getline(std::cin, delString);
                    Page *paj;
                    for(BagIterator<Page> it = pages.begin(); it!=pages.end(); it++)
                    {
                     if((*it).getName() == delString)
                         paj = &(*it);
                    }
                    if(paj != NULL){
                    try {
                        user.removeLike(*paj);
                    }
                    catch(std::exception &e)
                    {
                        std::cout << e.what() << std::endl;
                    }
                    }
                }
                else
                {
                    socialnetwork(userlist, pages, user);
                }
                break;
            }
            case 13:
            {
                std::cin.clear();
                break;
                return;
            }
            default:
            {
                std::cout << "Invalid input detected. Please choose again.\n";
                socialnetwork(userlist, pages, user);
            }
        }
    }
}

int main()
{
   int choice = 0;
    UserList* userlist = new UserList();
    PageList *pages = new PageList();
    int first_choice = 0;
    string file_dump = "";
    std::cout << "Welcome to the Social Network!" << std::endl;
    userlist->readListFromFile("example.txt");
        pages->readListFromFile("pages.txt");
        file_dump = "example.txt";
    bool endtimescenario = false;
    while(!endtimescenario)
    {
    cout << "Welcome to the Social Network beta! \n Would you like to: \n 1) Log in as a user? \n 2) Create a new user? \n 3) Quit the program?\n";
    cin >> choice;
        cin.ignore();
    if(cin)
    {
        switch(choice)
        {
            case 1:
            {
                string username = "";
                string password = "";
                bool loggedin = false;
                cout << "You selected: log in as a user. To log in as a user, please enter your username now.\n";
                getline(cin, username);
                cin.clear();
                cout << "Please enter your password now.\n";
                getline(cin, password);
                User* temp = temp = userlist->findUser(username);
                if(temp!=NULL)
                {
                    auto passwordHash = md5(password.c_str(), password.size()*sizeof(char));
                    password = passwordHash.toString();
                if(temp->getPassword()==password)
                    {
                    loggedin = true;
                    socialnetwork(*userlist, *pages, *temp);
                        if(first_choice!=1)
                        {
                    userlist->writeListToFile(file_dump.c_str());
                    pages->writeListToFile("pages.txt");
                        }
                    break;
                    }
                }
                else
                {
                    std::cout << "Incorrect username. Poor show." << std::endl;
                }
                if(!loggedin)
                {
                    cout << "Log in failed. Maybe your password was not correct.\n";
                }
                break;
            }
            case 2:
            {
                User user;
                string username, password, name, hometown;
                cout << "You selected: create new user. To create a new user, please enter a username to assign the new user." << endl;
                getline(cin, username);
                cin.clear();
                while(userlist->containsUser(username))
                {
                    cout << "This username is already taken. Please enter a new username.\n";
                    getline(cin, username);
                    cin.ignore();
                }
                cout << "Please assign a password for the new user." << endl;
                getline(cin, password);
                cin.clear();
                cout << "Please assign the user's real name for the new user." << endl;
                getline(cin, name);
                cin.clear();
                cout << "Please give a hometown for the new user. You may leave this blank if you wish.\n";
                getline(cin, hometown);
                auto hash = md5(password.c_str(), password.size()*sizeof(char));
                std::string passHash = hash.toString();
                User newUser(username, passHash, name, hometown);
                userlist->addUser(newUser);
                userlist->writeListToFile(file_dump.c_str());
                break;
            }
            case 3:
            {
                endtimescenario = true;
                break;
            }
            default:
            {
                cout << "Input is incorrect." << endl;
                endtimescenario = true;
                break;
            }
        }
    }
    else
    {
        
    }
    }
    delete userlist;
    delete pages;
}


