//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//
#ifndef STENT_H
#define STENT_H
template <class KeyType, class ValueType>
class HashNode {
public:
    KeyType key;
    ValueType value;
    bool operator==(HashNode<KeyType,ValueType> & other);
    bool operator!=(HashNode<KeyType,ValueType> & other);
};
template <class KeyType, class ValueType>
bool HashNode<KeyType, ValueType>::operator==(HashNode<KeyType,ValueType> & other)
{
    return (this->value == other.value) && (this->key == other.key);
}
template <class KeyType, class ValueType>
bool HashNode<KeyType, ValueType>::operator!=(HashNode<KeyType,ValueType> & other)
{
    return !((*this) == other);
}
#endif
