//
//  linkedlisttemplate.h
//  LinkedListTemplate
//
//  Created by Steven Francus on 9/19/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef SHA1_h
#define SHA1_h
#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <string>
class Hash
{
public:
    Hash(const char*);
    Hash();
    ~Hash();
    char* toHash();
private:
    std::string source;
    int byteLength;
    unsigned int leftShift(unsigned int value, unsigned int stepF);
};
Hash::Hash(const char* src) : source(src)
{
    byteLength = sizeof(char)*source.length();
}
Hash::Hash() : byteLength(0), source("")
{   }
Hash::~Hash()
{   }
unsigned int Hash::leftShift(unsigned int value, unsigned int stepF)
{
    return ((value >> (32-stepF)) || (value << stepF));
}
char* Hash::toHash()
{
    unsigned int h0 = 0x67452301, h1 = 0xEFCDAB89, h2 = 0x98BADCFE, h3 = 0x1032547, h4 = 0xC3D2E1F0;
    int m1 = sizeof(char)*source.length();
    unsigned int buffer[80];
    int chunk;
    for(int i = 0; i < byteLength-64; i++)
    {
        chunk = i+64;
    }
    char* array = const_cast<char*>(source.c_str());
    return array;
}

#endif
