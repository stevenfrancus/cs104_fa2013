#ifndef PAGE_H
#define PAGE_H
#include <iostream>
#include <sstream>
#include <string>
class Page
{
public:
    Page();
    ~Page();
    Page(std::string, std::string, std::string, std::string);
    Page(std::string, std::string);
    std::string getAbout();
    std::string getBlurb();
    std::string getName();
    std::string getCreator();
    void setCreator(std::string);
    void setAbout(std::string);
    void setBlurb(std::string);
    int getLikes();
    void addLike();
    void removeLike();
//    void addUser(std::string);
//   void removeUser(std::string);
    bool operator==(const Page &other)
    {
        return (this->name==other.name && this->blurb==other.blurb && this->about==other.about);
    };
    bool operator!=(const Page &other)
    {
        return !(*this==other);
    };
    std::string PageToFile();
    void PageFromFile(std::string);
    void printPage();
private:
    std::string creatorName;
    int likes;
    std::string name;
    std::string blurb;
    std::string about;
};
#endif