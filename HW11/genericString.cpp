//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#include "genericString.h"
genericString::genericString(std::string n) : strung(n) {};
genericString::genericString() : strung("") {};
std::string strung;
bool genericString::operator==(const genericString & other) const
{
    return this->strung == other.strung;
};
bool genericString::operator!=(const genericString &other) const
{
    return !(*this==other);
};
genericString& genericString::operator=(const genericString& p)
{
    this->strung = p.strung;
};
int genericString::hash(int _size) const //modified djb2 hash
{
    const char* aleph = strung.c_str();
    int c;
    unsigned long hash = 5381;
    while (c = *aleph++)
        hash = ((hash << 5) + hash) + c;
    return (hash % _size);
};
std::string genericString::toString()
{
    return strung;
}

