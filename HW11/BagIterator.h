//
//  BagIterator.h
//  
//
//  Created by Steven Francus on 10/28/13.
//
//

#ifndef _BagIterator_h
#define _BagIterator_h
#include "Node.h"
#include <iterator>
template <class T> class ADTBag;
template <class T>
class BagIterator: public std::iterator<std::bidirectional_iterator_tag, T>
{
public:
    BagIterator(const ADTBag<T> *list, Node<T>* node);
    bool operator!=(BagIterator const & other);
    bool operator==(BagIterator const & other);
    BagIterator operator++(int);
    BagIterator& operator++();
    T & operator*();
    friend class ADTBag<T>;
private:
    const ADTBag<T> *container;
    Node<T> *node;
};

#endif

