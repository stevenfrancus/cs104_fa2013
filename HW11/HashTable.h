//
//  main.cpp
//  HW9CS
//
//  Created by Steven Francus on 11/16/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//
#ifndef STUDENT_H
#define STUDENT_H
#include <vector>
#include <iostream>
#include <stdio.h>
#include <cmath>
#include "HashNode.h"
#include "HashTableIterator.h"
//#include "AbstractList.h"
#include <stdexcept>
template <class KeyType, class ValueType>
class Hashtable : protected std::vector< std::vector< HashNode<KeyType,ValueType> > > {
public:
    friend class HashTableIterator<KeyType,ValueType>;
    void add (const KeyType & key, const ValueType & value);
    void remove (const KeyType & key);
    ValueType & get (const KeyType & key);
    Hashtable (int initialSize);
    Hashtable ();
    HashTableIterator<KeyType,ValueType> begin();
    HashTableIterator<KeyType,ValueType> end();
private:
    void resizeHash();
    int primeFinder();
    int _size;
protected:
    int bucketSize(int) const;
    int hashSize() const;
    HashNode<KeyType,ValueType> & getNode(int, int);
};
#endif
