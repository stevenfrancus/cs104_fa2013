//
//  LinkedList.h
//  HW4CS
//
//  Created by Steven Francus on 9/29/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//
#include "LinkedList.h"
template <class T>
LinkedList<T>::LinkedList() : head(NULL), tail(NULL), lengthOf(0)
{
}
template <class T>
LinkedList<T>::LinkedList(Node<T>* headed) : head(headed), tail(headed)
{
}
template <class T>
void LinkedList<T>::insert (int pos, const T & item) throw(std::exception)
{
    if(pos < 0 || pos > lengthOf)
    {
        throw std::logic_error("Invalid position.");
    }
    Node<T>* newNode = new Node<T>;
    newNode->setData(item);
    if(pos==0)
    {
        newNode->setNext(head);
        head = newNode;
        lengthOf++;
        return;
    }
    int count = 0;
    for(Node<T> *addNode = head; addNode; addNode = addNode->getNext())
    {
        if(count==pos-1)
        {
            newNode->setNext(addNode->getNext());
            addNode->setNext(newNode);
            lengthOf++;
            break;
        }
        count++;
    }
    
};
template <class T>
void LinkedList<T>::remove (int pos) throw(std::exception)
{
    Node<T>* node;
    if(pos < 0 || pos > lengthOf)
    {
        throw std::logic_error("Invalid position.");
    }
    if(pos==0)
    {
        node = head;
        head = head->getNext();
        lengthOf--;
    }
    int count = 0;
    for(Node<T>* p = head; p; p = p->getNext())
    {
        if(count==pos-1)
        {
            node = p->getNext();
            p->setNext(node->getNext());
            lengthOf--;
        }
        count++;
    }
    node->setNext(NULL);
    delete node;
    node = NULL;
};
template <class T>
void LinkedList<T>::set (int pos, const T & item) throw(std::exception)
{
    if(pos < 0 || pos > lengthOf)
    {
        throw std::logic_error("Invalid position");
    }
    int count = 0;
    for(Node<T>* p = head; p->getNext(); p = p->getNext())
    {
        if(count==pos)
        {
            p->setData(item);
        }
        count++;
    }
};
template <class T>
T const & LinkedList<T>::get (int pos) const throw(std::exception)
{
    T* temp = new T();
    if(pos < 0 || pos > lengthOf)
    {
        throw std::logic_error("Invalid position");
    }
    int count = 0;
    for(Node<T>* p = head; p; p = p->getNext())
    {
        if(count==pos)
        {
            temp = &p->getData();
        }
        count++;
    }
    return *temp;
}
template <class T>
LinkedList<T>::~LinkedList()
{
    Node<T> *thisNode = head;
    while(thisNode)
    {
        Node<T>* nextNode;
        nextNode = thisNode->getNext();
        thisNode = nextNode;
    }
    head = NULL;
    delete thisNode;
}
template <class T>
int LinkedList<T>::length() const
{
    return lengthOf;
}
template <class T>
ListIterator<T> LinkedList<T>::begin()
{
    return ListIterator<T>(this, head);
}
template <class T>
ListIterator<T> LinkedList<T>::end()
{
    return ListIterator<T>(this, NULL);
    
}
