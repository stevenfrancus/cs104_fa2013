#include <string>
#include "UserList.h"
#include <fstream>
#include <iostream>
#include <istream>
#include <sstream>
#include <string>
#include <algorithm>
#include <exception>
#include <queue>
int firstIndexOfChar(std::string str, char character)
{
    for(int i = 0; i < str.length(); i++)
    {
        if(str[i]==character)
        {
            return i;
        }
    }
    return -1;
}
UserList::UserList()
{
}
UserList::~UserList()
{
}
void UserList::addUser(std::string user_name, std::string pass_word, std::string name, std::string home_town)
{
    User newUser(user_name, pass_word, name, home_town);
    genericString genera(user_name);
    userlist.add(genera, newUser);
    keyList.push_back(genera);
} 
void UserList::addUser(const User &user)
{
    genericString genera(user.getUsername());
    userlist.add(genera, user);
    keyList.push_back(genera);
}
void UserList::deleteUser(std::string username)
{
    genericString genera(username);
    userlist.remove(genera);
    keyList.erase(std::remove(keyList.begin(), keyList.end(), genera), keyList.end());
}
void UserList::writeListToFile(std::string filename)
{
    std::ofstream file;
    file.open(filename.c_str());
    for(int i = 0; i < keyList.size(); i++)
    {
        bool caught = false;
        User benutzer;
        try {
            benutzer = userlist.get(keyList[i]);
        }
        catch(std::exception &e)
        {
            caught = true;
        }
        if(!caught)
        {
        file << benutzer.userDataToString() + benutzer.writeWallToFile() + "^";
        }
    }
    file << "+";
    for(int i = 0; i < keyList.size(); i++)
    {
        bool caught = false;
        User benutzer;
        try {
            benutzer = userlist.get(keyList[i]);
        }
        catch(std::exception &e)
        {
            caught = true;
        }
        if(!caught)
        {
        file << benutzer.printFriends(false) + benutzer.printPending(false);
        }
    }
    file.close();
}
void UserList::readListFromFile(std::string filename)
//shit gets weird here yo
{
    std::ifstream aus;
    aus.open(filename.c_str(), std::ios::in);
    std::string friendOr = "";
    std::string linea = "";
    while(aus.good())
    {
        getline(aus, linea, '^');
        if(linea[0]=='%')
        {
            std::string userdata, username, password, nameD, hometown, likeList;
            linea = linea.substr(1, linea.length()-1);//strips off delimiters & and %
            int index = firstIndexOfChar(linea, '\n');
                //splits off first line
            userdata = linea.substr(0, index);
            linea = linea.substr(index+1, linea.length()); //contains pending friends,friends,and wall posts
            int username_index = firstIndexOfChar(userdata, '.');
            
            username = userdata.substr(0, username_index);
            userdata = userdata.substr(username_index+1, userdata.length()-1);
            nameD = userdata.substr(0, firstIndexOfChar(userdata, '.'));
            userdata = userdata.substr(firstIndexOfChar(userdata, '.')+1, userdata.length()-1);
            password = userdata.substr(0, firstIndexOfChar(userdata, '.'));
            userdata = userdata.substr(firstIndexOfChar(userdata, '.'), userdata.length()-1);
            
            hometown = userdata.substr(1, firstIndexOfChar(userdata, '.'));
            userdata = userdata.substr(firstIndexOfChar(userdata, '!')+1, userdata.length()-1);
            std::stringstream str(userdata);
            std::vector<std::string> likes;
            while(std::getline(str, likeList, '|'))
            {
                likes.push_back(likeList);
            }
            User user(username, password, nameD, hometown, likes);
            std::string wallpost = "";
            std::string pendingFriend = "";
            std::string friendName = "";
            std::stringstream ss(linea);
            while(std::getline(ss, wallpost, '*'))
            {
                if(wallpost.length())
                    user.stringToWall(wallpost);
            }
            addUser(user);
        }
           }
   aus.close();
    std::ifstream aut;
    aut.open(filename.c_str(), std::ios::in);
    while(aut.good())
    {
        while(std::getline(aut, friendOr, '+'))
        {
            if(friendOr[0]!='%')
            {
                std::string buffer = "";
                std::stringstream rr(friendOr);
                while(std::getline(rr,buffer))
                {
                    int checkPend = firstIndexOfChar(buffer,'>');
                    int checkFriend = firstIndexOfChar(buffer,':');
                    if(checkPend==-1)
                    {
                        std::string userOwn = buffer.substr(0, checkFriend);
                        std::string userAdd = buffer.substr(checkFriend+1, buffer.length()-1);
                        User *userOwne, *userAdde;
                        genericString generis(userOwn);
                        genericString generam(userAdd);
                        User benutzer, benutzersFreund;
                            try {
                                userOwne = &(userlist.get(generis));
                                userAdde = &(userlist.get(generam));
                            }
                            catch(std::exception &e)
                            {
                                
                            }
                        if(userOwne!=NULL && userAdde!=NULL)
                        {
                        userOwne->addFriend(*userAdde);
                        }
                    }
                    else if(checkFriend==-1)
                    {
                        std::string userOwn = buffer.substr(0, checkPend);
                        std::string userAdd = buffer.substr(checkPend+1, buffer.length()-1);
                        User *userOwne, *userAdde;
                        genericString generis(userOwn);
                        genericString generam(userAdd);
                        User benutzer, benutzersFreund;
                        try {
                            userOwne = &(userlist.get(generis));
                            userAdde = &(userlist.get(generam));
                        }
                        catch(std::exception &e)
                        {
                            
                        }
                        if(userOwne!=NULL && userAdde !=NULL)
                        {
                        userOwne->addPending(*userAdde);
                        }
                        
                    }
                }
            }
        }
    }
}//exactly 100 lines huehuehue : edit not anymore
bool UserList::containsUser(std::string user_name)
{
    genericString genera(user_name);
    User temp;
    try
    {
        temp = userlist.get(genera);
    }
    catch(std::exception &e)
    {
        return false;
    }
    return true;
}
User* UserList::findUser(std::string username)
{
    User* temp;
    genericString genera(username);
    try
    {
        temp = &(userlist.get(genera));
    }
    catch(std::exception &e)
    {
        temp = NULL;
    }
    return temp;
}
std::vector<User*> UserList::findByName(std::string name) //also works on partial name
{
    std::vector<User*> temp;
    std::transform(name.begin(), name.end(), name.begin(), ::tolower);
    for(int i = 0; i < keyList.size(); i++)
    {
        User *temporaire;
        try
        {
            temporaire = &(userlist.get(keyList[i]));
            std::string userName = temporaire->getName();
            if(userName.find(name)!=std::string::npos)
            {
                temp.push_back(temporaire);
            }
        }
        catch(std::exception &e)
        {
            break;
        }
    }
    return temp;
}
void UserList::shortestPath(std::string userU, std::string userV)
{
    if(userU == userV)
    {
        std::cout << "The distance from you to you is zero." << std::endl;
        return;
    }
    genericString nodeU(userU);
    genericString nodeV(userV);
    Hashtable<genericString, int> distances(37);
    Hashtable<genericString, User> predecessors(37);
    Hashtable<genericString, bool> visited(37);
    for(int i = 0; i < keyList.size(); i++)
    {
        visited.add(keyList[i], false);
        distances.add(keyList[i], 0);
    }
    User start, end;
    try {
        start = userlist.get(nodeU);
        end = userlist.get(nodeV);
    }
    catch(std::exception &e)
    {
        return;
    }
    visited.add(nodeU, true);
    distances.add(nodeU, 0);
    std::queue<User> q;
    q.push(start);
    while(!q.empty())
    {
        User a = q.front();
        q.pop();
        
        for(BagIterator<User> start = a.friendsBegin(); start != a.friendsEnd(); start++)
        {
            std::string friendName = (*start).getUsername();
            genericString genera(friendName);
            std::string Name = a.getUsername();
            genericString gener(Name);
            bool visitedP = false;
            try {
                visitedP = visited.get(genera);
            }
            catch(std::exception &e) { }
            if(!visitedP)
            {
                visited.add(genera, true);
                int distanceV = 0;
                try {
                    distanceV = distances.get(gener);
                }
                catch(std::exception &e) { }
                distanceV++;
                distances.add(genera, distanceV);
                predecessors.add(genera, a);
                q.push((*start));
            }
        }
    }
    int nodeDist = 0;
    try {
        nodeDist = distances.get(nodeV);
    }
    catch(std::exception &e) { std::cout << "not found" << std::endl; }
    if(nodeDist == 0)
    {
        std::cout << "There are no active connections between " << start.getName() << " and " << end.getName() << " :(." << std::endl;
        return;
    }
    if(nodeDist == 1)
    {
        std::cout << start.getName() << " is directly connected to " << end.getName() << "." << std::endl;
    }
    else{
    std::cout << start.getName() << " is connected to " << end.getName() << " by: " << std::endl;
    }
    User u;
    int count = 1;
    while(true)
    {
        try {
            u = predecessors.get(nodeV);
        }
        catch(std::exception &e){}
        if(u.getUsername() == userU)
        {
            break;
        }
        std::cout << count << ". " << u.getName() << " (" << u.getUsername() << ") ";
        genericString n(u.getUsername());
        nodeV = n;
        ++count;
    }
    std::cout << std::endl;
}
HashTableIterator<genericString, User> UserList::begin()
{
    return userlist.begin();
}
HashTableIterator<genericString, User> UserList::end(){
    return userlist.end();
}
