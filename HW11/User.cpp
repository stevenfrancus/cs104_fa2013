#include "User.h"
User::User() : username(""), name(""), password(""), hometown("")
{
    friends = new Bag<User>();
    pendingFriends = new Bag<User>();
}
User::User(std::string userstring)
{
    std::stringstream re(userstring);
    std::string pushback;
    int i = 0;
    while(std::getline(re, pushback, '.'))
    {
        if(i==0)
            username = pushback;
        else if(i==1)
            name = pushback;
        else if(i==2)
            password = pushback;
        else if(i==3)
            hometown = pushback;
        else if(i==4)
        {
            std::string best = "";
            std::stringstream ss(pushback);
            while(std::getline(ss, best, '%'))
            {
                if(best.length() > 0)
                pagesLiked.push_back(best);
            }
        }
        i++;
    }
}
User::User(std::string user_name, std::string pass_word) : username(user_name), password(pass_word), name(""), hometown("")
{
    friends = new Bag<User>();
    pendingFriends = new Bag<User>();
}
User::User(std::string user_name, std::string pass_word, std::string name, std::string home_town) : username(user_name), password(pass_word), name(name),hometown(home_town)
{
    friends = new Bag<User>();
    pendingFriends = new Bag<User>();
}
User::User(std::string user_name, std::string pass_word, std::string name, std::string home_town, std::vector<std::string>& lex) : username(user_name), password(pass_word), name(name),hometown(home_town), pagesLiked(lex)
{
    friends = new Bag<User>();
    pendingFriends = new Bag<User>();
}
User::~User()
{
}
void User::addComment(int pos, std::string username, std::string comment)
{
    wall.getPost(pos)->addComment(name, comment);
}
void User::deleteComment(int wallpostPos, int commentPos, std::string username)
{
    wall.getPost(wallpostPos)->deleteComment(commentPos, name);
}
void User::deleteComment(int wallpostPos, int commentPos)
{
    wall.getPost(wallpostPos)->deleteComment(commentPos);
}
void User::setHometown(std::string aleph)
{
    hometown = aleph;
}
void User::setPassword(std::string pass)
{
    password = pass;
}
void User::setName(std::string nomenklatura)
{
    name = nomenklatura;
}
std::string User::getHometown() const
{
    return hometown;
}
std::string User::getUsername() const
{
    return username;
}
std::string User::getName() const
{
    return name;
}
std::string User::getPassword() const
{
    return password;
}
void User::stringToWall(std::string strung)
{
    wall.StringToWall(strung);
}
void User::writePost(std::string postBody)
{
    time_t rawtime;
    WallPost alpha(name, postBody);
    alpha.setTimeCreated(time(&rawtime));
    wall.addPost(alpha);
}
void User::writePost(std::string author, std::string postBody)
{
    time_t rawtime;
    WallPost omega(author, postBody);
    omega.setTimeCreated(time(&rawtime));
    wall.addPost(omega);
}
bool sortProper(WallPost& a, WallPost &b)
{
    return (a.getTimeCreated() <= b.getTimeCreated());
}
bool sortByMostRecent(WallPost& a, WallPost &b)
{
    return (a.mostRecentTime() <= b.mostRecentTime());
}
void User::quickSort(int chosa)
{
    bool (*comparison)(WallPost&, WallPost&);
    if(chosa == 1)
    {
        comparison = sortProper;
    }
    else if(chosa == 2)
    {
        comparison = sortByMostRecent;
    }
    wall.quickSort(comparison, 0, wall.length()-1);
}
void User::deletePost(int number)
{
    try {
    wall.removePost(number);
    }
    catch(std::exception &e)
    {
        std::cout << e.what() << std::endl;
    }
}
void User::deletePost(std::string username, int number)
{
    try {
    wall.removePost(username, number);
    }
    catch(std::exception &e) { std::cout << e.what() << std::endl; }
}
std::string User::userDataToString() const
{
    std::string varchar = "";
    for(int i = 0; i < pagesLiked.size(); i++)
    {
        varchar = varchar + pagesLiked[i] + "|";
    }
    return "%" + username + "." + name + "." + password + "." + hometown + "!" + varchar + "\n";
}
void User::outputUserData() const
{
    std::cout << "User info:\nUsername: " << username << "\nName: " << name << "\nHometown: "<< hometown << std::endl;
}
void User::printWall()
{
    wall.WallToString(true);
}
std::string User::writeWallToFile()
{
    return wall.WallToString(false);
}
bool User::operator==(User const & other)
{
    return this->username == other.username;
}
void User::addFriend(const User& user)
{
    friends->push_back(user);
}
void User::removeFriend(User &user)
{
            friends->remove(user);
            user.friends->remove(*this);
}
void User::addPending(const User &user)
{
    pendingFriends->push_back(user);
}
void User::sendFriendRequest(User &user)
{
    if(!user.pendingFriends->contains(*this) && !user.friends->contains(*this))
    {
    user.pendingFriends->push_back(*this);
    }
}
void User::writePostOnFriendWall(std::string postbody, User &frienduser)
{
    if(frienduser.friends->contains(*this) && this->friends->contains(frienduser))
    {
        WallPost beta(username, postbody);
        frienduser.wall.addPost(beta);
    }
}
void User::respondToFriendRequest(User &dum, bool response)
{
    if(response)
    {
        if(!friends->contains(dum))
        {
            friends->push_back(dum);
            dum.friends->push_back(*this);
        }
    }
    pendingFriends->remove(dum);
    if(dum.pendingFriends->contains(*this))
    {
        dum.pendingFriends->remove(*this);
    }
}
std::string User::printPending(bool writeOrPrint)
{
    std::string varchar ="";
    for(BagIterator<User> mu = pendingFriends->begin(); mu!=pendingFriends->end(); mu++)
    {
        if(writeOrPrint)
        std::cout << (*mu).getUsername() << std::endl;
        else
        {
            varchar= varchar + getUsername() + ">" + (*mu).getUsername();
            varchar+="\n";
        }
    }
    return varchar;
}
std::string User::printFriends(bool writeOrPrint)
{
    std::string varchar = "";
    for(BagIterator<User> mu = friends->begin(); mu!=friends->end(); mu++)
    {
        if(writeOrPrint)
        {
        std::cout << (*mu).getUsername() << std::endl;
        }
        else
        {
            varchar= varchar + getUsername() + ":" + (*mu).getUsername();
            varchar+="\n";
        }
    }
    return varchar;
}
void User::createPage(PageList& pages, std::string name, std::string blurb, std::string about)
{
    Page newPage(name, blurb, about, getUsername());
    newPage.addLike();
    pages.addPage(newPage);
    like(newPage);
}
void User::deletePage(PageList& pages, std::string pageName)
{
    bool a = false;
    for(BagIterator<Page> it = pages.begin(); it!=pages.end(); it++)
    {
        if((*it).getName()==pageName && (*it).getCreator() == getUsername())
        {
            removeLike((*it));
            pages.deletePage((*it));
            a = true;
            break;
        }
    }
    if(!a)
    {
        throw std::logic_error("Either the page you want to access does not exist or you do not have permission to delete it.");
    }
}
void User::like(Page& page)
{
    std::string pageN = page.getName();
    bool found = false;
    for(int i = 0; i < pagesLiked.size(); i++)
    {
        if(pageN == pagesLiked[i])
        {
            found = true;
            break;
        }
    }
    if(found)
    {
        std::cout << "You have already liked this page." << std::endl;
    }
    else {
    page.addLike();
    pagesLiked.push_back(pageN);
        }
}
void User::removeLike(Page& page)
{
    Page paj;
    if(page == paj) return;
    for(int i = 0; i < pagesLiked.size(); i++)
    {
        if(page.getName() == pagesLiked[i])
        {
            page.removeLike();
            pagesLiked.erase(pagesLiked.begin()+i);
            return;
        }
    }
    throw std::logic_error("Page not found.");
    
}
void User::printPagesLiked()
{
    int size = pagesLiked.size();
    for(int i = 0; i < size; i++)
    {
        std::cout << pagesLiked[i] << std::endl;
    }
}
BagIterator<User> User::friendsBegin()
{
    return friends->begin();
}
BagIterator<User> User::friendsEnd()
{
    return friends->end();
}