//
//  ListIterator.h
//  
//
//  Created by Steven Francus on 10/28/13.
//
//

#ifndef _ListIterator_h
#define _ListIterator_h
#include <vector>
#include <iterator>
#include "HashNode.h"
#include <iostream>
template <class KeyType, class ValueType> class Hashtable;
template <class KeyType, class ValueType>
class HashIterator: public std::iterator<std::bidirectional_iterator_tag, HashNode<KeyType,ValueType> >
{
public:
    HashIterator(Hashtable<KeyType,ValueType> const *list, HashNode<KeyType,ValueType> *val);
    bool operator!=(HashIterator const & other);
    bool operator==(HashIterator const & other);
    HashIterator operator++(int);
    HashIterator& operator++();
    const ValueType & operator*();
    friend class Hashtable<KeyType,ValueType>;
private:
    const Hashtable<KeyType,ValueType> *container;
    HashNode<KeyType,ValueType> *node;
    int pos;
};
template <class KeyType, class ValueType>
HashIterator<KeyType,ValueType>::HashIterator(Hashtable<KeyType,ValueType> const *list, HashNode<KeyType,ValueType> *n) : container(list), node(n)
{
    
};
template <class KeyType, class ValueType>
bool HashIterator<KeyType,ValueType>::operator!=(HashIterator const & other )
{
    return node!=other.node;
};
template <class KeyType, class ValueType>
bool HashIterator<KeyType,ValueType>::operator==(HashIterator const & other)
{
    return !(node!=other.node);
}
template <class KeyType, class ValueType>
HashIterator<KeyType,ValueType> & HashIterator<KeyType,ValueType>::operator++()
{
    int getIndexOf;
    for(int i = 0; (*container)[pos].size(); i++)
    {
        if((*container)[pos][i].key == node.key) getIndexOf = i;
    }
    if(getIndexOf = (*container)[pos].size()-1)
    {
        ++pos;
        if (pos > container->capacity()) return NULL;
        HashIterator hashe(container, (*container)[pos][0]);
        return *hashe;
    }
    else
    {
        return *this;
    }
};
template <class KeyType, class ValueType>
const ValueType & HashIterator<KeyType,ValueType>::operator*()
{
    KeyType val;
    for(int i = 0; (*container)[pos].size(); i++)
    {
        if((*container)[pos][i].key == node.key) val = (*container)[pos][i].value;
            }
    return val;
};
#endif