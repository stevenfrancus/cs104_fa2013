//
//  User.h
//  LinkedListTemplate
//
//  Created by Steven Francus on 9/21/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef LinkedListTemplate_User_h
#define LinkedListTemplate_User_h
#include "Wall.h"
#include "Pages.h"
#include "PageList.h"
#include "BagIterator.h"
#include "linkedlisttemplate.h"
#include <exception>
class User
{
public:
    User();
    User(std::string userstring);
    User(std::string user_name, std::string pass_word);
    User(std::string user_name, std::string pass_word, std::string name, std::string home_town);
    User(std::string, std::string, std::string, std::string, std::vector<std::string>&);
    ~User();
    std::string getHometown() const;
    std::string getName() const;
    std::string getUsername() const;
    std::string getPassword() const;
    void stringToWall(std::string);
    void setHometown(std::string);
    void setPassword(std::string);
    void setName(std::string);
    void writePost(std::string);
    void writePost(std::string, std::string); //for writing posts to other users' walls
    void deletePost(int number);
    void deletePost(std::string, int number); //for deleting your posts on other users' walls
    void deleteComment(int wallpostPos, int commentPos, std::string username);
    void deleteComment(int wallpostPos, int commentPos);
    void outputUserData() const;
    void printWall();
    void addFriend(const User&);
    void removeFriend(User&); 
    void addPending(const User&);
    void sendFriendRequest(User&);
    void respondToFriendRequest(User&, bool repons);
    void quickSort(int); //sort's User's wall according to choice int
    std::string printPending(bool);
    std::string printFriends(bool);
    std::string userDataToString() const;
    std::string writeWallToFile();
    void writePostOnFriendWall(std::string postbody, User &frienduser);
    void addComment(int pos, std::string username, std::string comment);
    bool operator==(User const & other);
    void createPage(PageList&, std::string, std::string, std::string);
    void deletePage(PageList&, std::string);
    void like(Page&);
    void removeLike(Page&);
    void printPagesLiked();
    BagIterator<User> friendsBegin();
    BagIterator<User> friendsEnd();
private:
    Wall wall;
    ADTBag<User>* friends;
    ADTBag<User>* pendingFriends;
    std::vector<std::string> pagesLiked;
    std::string username;
    std::string name;
    std::string password;
    std::string hometown;
    
};

#endif
