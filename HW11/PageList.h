#ifndef PAGELIST_H
#define PAGELIST_H
#include <iostream>
#include <sstream>
#include <string>
#include "Pages.h"
#include "BagIterator.h"
#include "linkedlisttemplate.h"
#include "ADTBag.h"
#include <fstream>
#include <istream>
class PageList
{
public:
    PageList();
    ~PageList();
    void addPage(Page&);
    void addPage(std::string, std::string, std::string, std::string);
    void deletePage(Page&);
    void deletePage(std::string, std::string);
    void printAllPages();
    BagIterator<Page> begin();
    BagIterator<Page> end();
    void writeListToFile(std::string);
    void readListFromFile(std::string);
private:
    ADTBag<Page>* pages;
};

#endif