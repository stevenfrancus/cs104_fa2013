#include <ctime>
#include "wallpostComment.h"
#include "LinkedList.h"
#include "LinkedListImp.cpp"
#include <vector>
#ifndef LinkedListTemplate_wallpost_h
#define LinkedListTemplate_wallpost_h

class WallPost
{
	public:
	WallPost(std::string a, std::string b);
    WallPost(std::string, std::string, int);
	WallPost();
	~WallPost();
    int mostRecentTime() const; //gets most recent time of comment
	void setBody(std::string);
	std::string getBody() const;
	void setAuthor(std::string);
	std::string getAuthor() const;
	void printWallPost() const;
    std::string formatWallPost() const;
	int getPostNumber() const;
	void setPostNumber(int);
    int getTimeCreated() const;
	void setTimeCreated(int);
    void addComment(std::string, std::string);
    void addComment(wallpostComment &);
    void deleteComment(int);
    void deleteComment(int, std::string);
    
	private:
	std::string body;
	std::string authorname;
    int datetime;
	int postNumber;
    std::vector<wallpostComment> comments;
};
#endif