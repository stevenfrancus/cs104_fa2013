//
//  UserList.h
//  LinkedListTemplate
//
//  Created by Steven Francus on 9/21/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef LinkedListTemplate_UserList_h
#define LinkedListTemplate_UserList_h
#include "HashTable.h"
#include "HashTableImp.cpp"
#include "genericString.h"
#include "User.h"
#include "Node.h"
#include "HashTableIterator.h"
#include <fstream>
#include <iostream>
#include <queue>
#include <istream>
#include <sstream>
class UserList
{
public:
    UserList();
    ~UserList();
    //void socialnetwork(User& user);
    void addUser(const User &user);
    void addUser(std::string user_name, std::string name, std::string pass_word, std::string home_town);
    void deleteUser(std::string user);
    void writeListToFile(std::string filename);
    void readListFromFile(std::string filename);
    bool containsUser(std::string username);
    User* findUser(std::string username);
    std::vector<User*> findByName(std::string name);
    void shortestPath(std::string, std::string);
    HashTableIterator<genericString, User> begin();
    HashTableIterator<genericString, User> end();
    
private:
    Hashtable<genericString,User> userlist;
    std::vector<genericString> keyList;
    
};

#endif
