//
//  ListIterator.h
//  
//
//  Created by Steven Francus on 10/28/13.
//
//

#ifndef _ListIterator_h
#define _ListIterator_h
#include "Node.h"
#include <iterator>
template <class T>
class ListIterator: public std::iterator<std::bidirectional_iterator_tag, T>
{
public:
    ListIterator(const List<T> *list, Node<T>* node);
    bool operator!=(ListIterator const & other);
    bool operator==(ListIterator const & other);
    ListIterator operator++(int);
    ListIterator& operator++();
    const T & operator*();
    friend class List<T>;
private:
    const List<T> *container;
    Node<T> *node;
};
template <class T>
ListIterator<T>::ListIterator(const List<T> *list, Node<T>*n) : container(list), node(n)
{
    
};
template <class T>
bool ListIterator<T>::operator!=(ListIterator const & other )
{
    return node!=other.node;
};
template <class T>
bool ListIterator<T>::operator==(ListIterator const & other)
{
    return !(node!=other.node);
}
template <class T>
ListIterator<T> & ListIterator<T>::operator++()
{
    node = node->getNext();
    return *this;
};
template <class T>
ListIterator<T> ListIterator<T>::operator++( int )
{
    ListIterator<T> current( *this );
    node = node->getNext();
    return current;
};
template <class T>
const T & ListIterator<T>::operator*()
{
    return node->getData();
};
#endif