#ifndef PAGEIMP_H
#define PAGEIMP_H
#include "Pages.h"
Page::Page() : likes(0), creatorName(""), name(""), blurb(""), about("")
{
   // users = new Bag<std::string>();
}
Page::Page(std::string Name, std::string Blurb, std::string About, std::string creatorname) : likes(0), name(Name), blurb(Blurb), about(About), creatorName(creatorname)
{
   // users = new Bag<std::string>();
}
Page::Page(std::string setName, std::string creatorname) : likes(0), name(setName), creatorName(creatorname)
{
   // users = new Bag<std::string>();
}
Page::~Page()
{
    //delete users;
}
std::string Page::getAbout()
{
    return about;
}
std::string Page::getBlurb()
{
    return blurb;
}
std::string Page::getName()
{
    return name;
}
std::string Page::getCreator()
{
    return creatorName;
}
void Page::setCreator(std::string thisstring)
{
    creatorName = thisstring;
}
void Page::setAbout(std::string aboot)
{
    about = aboot;
}
void Page::setBlurb(std::string blarb)
{
    blurb = blarb;
}
int Page::getLikes()
{
    return likes;
}
void Page::printPage()
{
    std::cout << "~~~~~~" << name << "~~~~~~" << std::endl;
    if(likes == 1)
    {
        std::cout << likes << " person likes this." << std::endl;
    }
    else{
    std::cout << likes << " people like this." << std::endl;
    }
    std::cout << blurb << std::endl;
    std::cout << "::::::About::::::" << std::endl;
    std::cout << about << std::endl;
}
std::string Page::PageToFile()
{
    std::stringstream ss;
    ss << likes;
    std::string _likes = ss.str();
    return name + ":" + blurb + ":" + about + ":" + _likes + ":" + creatorName + "`";
}
void Page::PageFromFile(std::string filestring)
{
    std::string buffer = "";
    int counter = 0;
    std::stringstream ss(filestring);
    while(std::getline(ss, buffer, ':'))
    {
        switch(counter)
        {
            case 0:
            {
                name = buffer;
                break;
            }
            case 1:
            {
                blurb = buffer;
                break;
            }
            case 2:
            {
                about = buffer;
                break;
            }
            case 3:
            {
                std::stringstream str(buffer);
                int _likes = 0;
                str >> _likes;
                likes = _likes;
                break;
            }
            case 4:
            {
                creatorName = buffer;
            }
            default:
            {
                break;
            }
        }
        ++counter;
    }
}
void Page::addLike()
{
    likes++;
}
void Page::removeLike()
{
    likes--;
}
#endif
