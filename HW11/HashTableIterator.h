//
//  BagIterator.h
//  
//
//  Created by Steven Francus on 10/28/13.
//
//

#ifndef _HashIterator_h
#define _HashIterator_h
#include "HashNode.h"
#include <iterator>
template <class KeyType, class ValueType> class Hashtable;
template <class KeyType, class ValueType>
class HashTableIterator: public std::iterator<std::bidirectional_iterator_tag, ValueType>
{
public:
    HashTableIterator(Hashtable<KeyType,ValueType> *table, int, int);
    bool operator!=(HashTableIterator const & other);
    bool operator==(HashTableIterator const & other);
    HashTableIterator operator++(int);
    HashTableIterator& operator++();
    HashNode<KeyType,ValueType> & operator*();
    friend class Hashtable<KeyType, ValueType>;
private:
    Hashtable<KeyType, ValueType> *container;
    int bucket;
    int val;
};
template <class KeyType, class ValueType>
HashTableIterator<KeyType,ValueType>::HashTableIterator(Hashtable<KeyType,ValueType> *table, int firstPos, int secondPos) : container(table), bucket(firstPos), val(secondPos) {}
template <class KeyType, class ValueType>
bool HashTableIterator<KeyType, ValueType>::operator!=(HashTableIterator const & other)
{
    return (this->bucket != other.bucket) && (this->val != other.val);
}
template <class KeyType, class ValueType>
bool HashTableIterator<KeyType, ValueType>::operator==(HashTableIterator const & other)
{
    return (this->bucket == other.bucket) && (this->val == other.val);
}
template <class KeyType, class ValueType>
HashTableIterator<KeyType,ValueType> HashTableIterator<KeyType,ValueType>::operator++(int a)
{
    HashTableIterator<KeyType,ValueType> current( *this );
    if(bucket == container->hashSize()-1) {
        bucket = -1; val = -1; }
    else if(val == container->bucketSize(bucket)-1) {
        bucket++; val = 0;
        while(container->bucketSize(bucket)==0)
        {
            bucket++;
        }
    }
    else {
        val++;
    }
    return current;
}
template <class KeyType, class ValueType>
HashTableIterator<KeyType,ValueType> & HashTableIterator<KeyType,ValueType>::operator++()
{
    if(bucket == container->hashSize()-1) { bucket = -1; val = -1; }
    else if(val == container->bucketSize(bucket)-1) { bucket++; val = 0;
        while(container->bucketSize(bucket)==0 && bucket < container->hashSize())
        {
            bucket++;
        }
    }
    else {
        val++;
    }
    return *this;
}
template <class KeyType, class ValueType>
HashNode<KeyType,ValueType> & HashTableIterator<KeyType,ValueType>::operator*()
{
    return container->getNode(bucket, val);
}


#endif

