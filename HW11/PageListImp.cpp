#ifndef PAGELIST
#define PAGELIST
#include "PageList.h"
PageList::PageList()
{
    pages = new Bag<Page>();
};
PageList::~PageList()
{
    delete pages;
};
void PageList::addPage(Page& page)
{
    pages->push_back(page);
};
void PageList::addPage(std::string name, std::string blurb, std::string about, std::string creatorN)
{
    Page pagina(name, blurb, about, creatorN);
    pages->push_back(pagina);
};
void PageList::deletePage(Page & page)
{
    pages->remove(page);
};
void PageList::deletePage(std::string name, std::string creator)
{
    for(BagIterator<Page> p = begin(); p!= end(); p++)
    {
        if((*p).getName() == name && (*p).getCreator() == creator)
        {
            pages->remove((*p));
        }
    }
};
void PageList::printAllPages()
{
    for(BagIterator<Page> p = begin(); p != end(); p++)
    {
        (*p).printPage();
    }
};
BagIterator<Page> PageList::begin()
{
    return pages->begin();
};
BagIterator<Page> PageList::end()
{
      return pages->end();
};
void PageList::writeListToFile(std::string filename)
{
    std::string file_string = "";
    for(BagIterator<Page> p = begin(); p != end(); p++)
    {
        file_string += (*p).PageToFile();
    }
    std::ofstream file;
    file.open(filename.c_str());
    file << file_string;
    file.close();
};
void PageList::readListFromFile(std::string filename)
{
    std::ifstream aus;
    aus.open(filename.c_str(), std::ios::in);
    std::string fileread = "";
    while(aus.good())
    {
        getline(aus, fileread, '`');
        if(fileread == "") { break; }
        Page paj;
        paj.PageFromFile(fileread);
        pages->push_back(paj);
    }
};
#endif