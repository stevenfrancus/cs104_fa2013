//
//  linkedlisttemplateImp.cpp
//  
//
//  Created by Steven Francus on 10/28/13.
//
//

/*#include "BagIterator.h"
#include "linkedlisttemplate.h"
template <class T>
Bag<T>::Bag() : head(NULL), tail(NULL), sizeOf(0)
{
    
}
template <class T>
Bag<T>::Bag(Node<T>* headed) : head(headed), tail(headed), sizeOf(1)
{
    
}
template <class T>
Bag<T>::~Bag<T>()
{
    while(head)
    {
        Node<T>* nextNode;
        nextNode = head->getNext();
        delete head;
        head = nextNode;
    }
    head = NULL;
    delete head;
}
template <class T>
void Bag<T>::push_back(const T &item)
{
    Node<T> *p = new Node<T>;
	p->setData(item);
	p->setNext(NULL);
	p->setPrev(tail);
	if (!head)
		head = tail = p;
	else {
        tail->setNext(p);
        tail = p;
    }
    sizeOf++;
    
}
template <class T>
bool Bag<T>::contains(const T & item)
{
    for(BagIterator<T> mu = begin(); mu!=end(); mu++)
    {
        if((*mu)==item)
        {
            return true;
        }
    }
    return false;
}
template <class T>
void Bag<T>::remove (T &item)
{
	Node<T> *p;
	p = head;
	while (p)
	{
		if (p->getData() == item)
		{
			Node<T> *q = p->getNext();
            if (p == head)
                head = p->getNext();
            else p->getPrev()->setNext(p->getNext());
            if (p == tail)
                tail = p->getPrev();
            else p->getNext()->setPrev(p->getPrev());
            //delete p;
			p = q;
		}
		else p = p->getNext();
	}
    sizeOf--;
    
}
template <class T>
int Bag<T>::size()
{
    return sizeOf;
}
template <class T>
BagIterator<T> Bag<T>::begin()
{
    return BagIterator<T>(this, head);
}
template <class T>
BagIterator<T> Bag<T>::end()
{
    return BagIterator<T>(this, NULL);
    
}
