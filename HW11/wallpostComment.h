//
//  linkedlisttemplate.h
//  LinkedListTemplate
//
//  Created by Steven Francus on 9/19/13.
//  Copyright (c) 2013 USC 104. All rights reserved.
//

#ifndef LinkedListTemplate_wallpostcomment_h
#define LinkedListTemplate_wallpostcomment_h
#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <string>
#include <time.h>
class wallpostComment
{
public:
    wallpostComment(std::string, std::string);
    wallpostComment();
    ~wallpostComment() { }
    std::string getCommentBody() const;
    void setCommentBody(std::string);
    std::string getPoster() const;
    void setPoster(std::string);
    void setTimeCreated(int);
    int getTimeCreated() const;
private:
    std::string commentBody;
    std::string username;
    int time;
    
};
#endif
