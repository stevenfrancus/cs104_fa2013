//
//  linkedlisttemplateImp.cpp
//  
//
//  Created by Steven Francus on 10/28/13.
//
//

#include "ListIterator.h"
template <class T>
BagIterator<T>::BagIterator(const ADTBag<T> *bag, Node<T>*n) : container(bag), node(n)
{
    
};
template <class T>
bool BagIterator<T>::operator!=(BagIterator const & other )
{
    return node!=other.node;
};
template <class T>
bool BagIterator<T>::operator==(BagIterator const & other)
{
    return !(node!=other.node);
}
template <class T>
BagIterator<T> & BagIterator<T>::operator++()
{
    node = node->getNext();
    return *this;
};
template <class T>
BagIterator<T> BagIterator<T>::operator++( int )
{
    BagIterator<T> current( *this );
    node = node->getNext();
    return current;
};
template <class T>
T & BagIterator<T>::operator*()
{
    return node->getData();
};
